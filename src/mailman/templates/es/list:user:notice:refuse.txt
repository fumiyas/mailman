Su petición a la lista de correo de $listname

    $request

ha sido rechazada por el moderador de la lista.  El moderador dio el
siguiente motivo para rechazar su solicitud:

"$reason"

Cualquier pregunta u observación debe dirigirse al administrador de la lista
en:

    $owner_email
