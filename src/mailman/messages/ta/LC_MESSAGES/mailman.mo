��    �       -  �      �"  �  �"  a  �$     �&     '     '     '     +'     <'     R'  =   j'  �   �'  T   *(  �  (  �   C*    +  ;    -  �   \-  c   �-  �   X.  �   /     0  }   %0  �   �0  '   �1  �   �1  �   _2  Y   3  �   m3  �   84  �   /5  �   �5  �  �6  p   �8  *   9  g   >9  ^   �9  \   :  $  b:  Y   �;  o   �;  s   Q<  _   �<  G   %=     m=  q  �=  �  �>  �  �@  C   �B  �   �B  �   �C  �   7D  �   �D  �   PE    �E     �I  y   J  �  �J  P   pM  I   �M  �   N  �   �N  �   �O  :   �P  P   �P  J   2Q  9   }Q  �   �Q  �   �R  �   \S  {   �S  b   \T  ^   �T  R   U  �   qU  �   =V  g   +W  >  �W     �X  �   �X  �  �Y  q   >\     �\  �   �\  I   �]  �   �]  t   �^  J   _  �   \_  o   Q`  w   �`  �   9a     �a  &   b  #   )b  <   Mb  8   �b  M   �b  =   c  :   Oc  C   �c  0   �c  /   �c  1   /d  @   ad  U   �d     �d  Q   e  P   je     �e  4   �e  %   
f  %   0f  G   Vf  J   �f  K   �f  G   5g  @   }g  1   �g  9   �g  .   *h  8   Yh     �h     �h  (   �h  3   �h  #   i  �   <i  K   �i     j     2j  7   Dj      |j      �j  3   �j     �j  (   k  (   :k     ck  0   yk  .   �k  '   �k     l     l     *l  <   @l  �   }l  !   Gm     im  �  �m  D   To     �o  -   �o  0   �o  *   p  7   =p  6   up  8   �p  8   �p  )   q  2   Hq  '   {q  1   �q  #   �q  *   �q  /   $r  $   Tr  D   yr      �r  	   �r  #   �r  ,   s     :s  ,   Ks     xs     �s  &   �s  4   �s     �s  3   t     ?t  	   Yt  /   ct     �t  $   �t  "   �t  |   �t     `u     }u  C   �u     �u  +   �u  *   'v     Rv     lv  (   �v  $   �v  8   �v  1   w  !   Ew    gw  #   y  m  �y     {  "   0{  #   S{  #   w{  (   �{  9   �{     �{     |  '   9|  -   a|  '   �|     �|     �|  (   �|     }  C   -}     q}     �}  %   �}  :   �}  )   �}     #~     ,~     E~  d   e~  (   �~  )   �~  "     %   @  *   f  "   �  )   �     �     �     �  ,   -�      Z�  #   {�     ��  !   ��  *   ؀     �     !�     2�  ,   J�     w�     {�  Q   ��  A   ݁  S   �     s�     ��     ��  .   ɂ     ��  #   �      2�     S�  '   o�     ��     ��     ȃ  4   ��     �     /�  2   =�  8   p�     ��  '   ��     �  %   ��  4   �  =   Q�  1   ��  C   ��  %   �  +   +�  %   W�     }�     ��      ��  +   φ  #  ��  y  �     ��  0   ��  !   ׉  ,   ��     &�     A�     X�     r�  2   ��     Ê  $   �     �  '   !�     I�  2   U�     ��     ��  6   ��      ��  %   �  8   ?�     x�  .   ��      ��  1   ��     �  -   ,�     Z�  1   g�     ��  -   ��     ܍      ��     �  �   +�  "   �  "   5�     X�     w�     ��  }   ��  w   1�  e   ��    �  �   '�  )   ��  6   ޒ      �  1   6�  %   h�  4   ��  5   Ó  6   ��  7   0�  6   h�  "   ��  &     0   �     �  /   5�  G  e�  V   ��  "   �     '�     8�     U�     q�  &   ��  '   ��     ߗ     �  	   �  =   �    P�  2   h�  �   ��  L   n�  F   ��  u  �  R   x�  S   ˜  @   �  '   `�  J   ��  �   ӝ     m�     ��     ��     ��     Þ  !   ̞     �     	�     �     .�      H�     i�  /   r�     ��     ��  !   ݟ     ��     �     =�     [�     y�  "   ��  !   ��     ޠ     ��     �     9�      X�     y�  "   ��     ��     С     �     �     %�     B�     _�     c�     q�  *   ��     ��  �  ��  $  d�  s  ��     ��  =   �     [�  4   {�  I   ��  C   ��  *   >�  �   i�  Y  ��  �   Y�  "  Y�  u  |�  �  �  �   ��  �  ��    d�  6  z�  Y  ��     �  z  &�  4  ��  �   ��  �  d�    ��  F  ��  H  F�  �  ��  7  |�  �  ��  �  w�  �  *�  �   ��  2  F�  �   y�  <  a�    ��  �   ��  �  ��  e  >�  5  ��  �   ��  $   ��  �  ��  �  ��  �  G�  �   � �  � "  � �  � h  �
 �   �  � 0   � |  �   Y &  x$ �   �% �  ~& �  ) �  �+ �   F. �   �. �   �/ t   M0 �  �0 k  P3 �  �5 �  o7 t  '9 C  �: �   �; {  �<   B? H  YB (  �C h   �G �  4H �  J R  �Q #   S t  @S �   �U �  rV m  X   �Y   �Z �  �] �  O_ v  a 1   �c �   �c <   Rd e   �d f   �d �   \e Q   f �   qf w   &g e   �g B   h K   Gh �   �h �   i I   �i    �i �   sj 8   �j    /k /   Nk 6   ~k �   �k �   [l �   m �   �m t   wn �   �n �   uo Y   p G   ap $   �p >   �p A   q �   Oq X   r ?  jr   �s ^   �t Z   u �   gu k   /v i   �v �   w e   �w h   �w j   _x =   �x    y �   �y W   "z \   zz >   �z b   { �   y{ �  | z   �} \   *~    �~ �   �� C   7� �   {� �   )� z   �� �   *� }   � �   _� �   � |   �� �   >� �   �� �   �� �   :� �   � �   �� �   O� �   ߍ t   ~� ?   � `   3� c   ��    �� �   � _   ϐ +   /� �   [� �   � T   �� �   � p   }� "   � �   �    �� n   �� x   *� /  �� J   Ӗ f   � �   �� M   � �   m� �   � �   t� �   �� �   x� v   -� �   �� �   Y� x   �� �  n� �   D� x  ȣ H   A� }   �� W   � c   `� n   ĩ �   3� N   � L   [� H   �� �   � S   �� <   ֬ U   � l   i� h   ֭ 	  ?� =   I� �   �� T   � �   b� �   G�    � ]   � ^   a� 	  �� �   ʳ �   {� �   � �   �� �   9� a   ¶ �   $�    �� J   ѷ Z   � �   w� `   � r   g� T   ڹ c   /� �   �� [   D� (   �� N   ɻ �   �    ��    ͼ �   � �   s� �   � ?   �� �   �� �   �� �   "� W   �� |   � J   �� v   �� �   S� I   �� L   $� @   q� i   �� _   � 5   |� �   �� �   �� M   R� t   ��    � S   5� �   �� �   S� �   � �   �� u   �� �   9� s   �� Z   5� L   �� Q   �� �   /�   �� �  ��    �� �   �� a   w� �   �� R   �� I   �� H   )� �   r� �   � �   �� ^   .� T   ��    �� #   b� �   �� Y   |� P   �� �   '� n   �� c   ?� �   �� L   �� �   �� `   �� �   �� ]   �� �   �    �� �   �� (   \� e   �� D   ��    0� ;   �� }  �� �   j� x   �� W   j� `   �� c   #� �  �� ?  �   H� %  b� u  �� �   �� �   �� i   U� ^   �� r   � �   �� �   2� �   �� �   �� �   3� o   �� `   V� I   �� 3   � �   5� �  ��   Y� Y   `� 3   �� P   �� L   ?� Z   �� d   �� �   L  G   �  1       G P   \ �  � �   t �  � �   � �   � u  8 �   � �   U �    a   � �   B T  �    C 6   ] ?   �    �    � q   � [   ` [   � d    0   } X  � 3   �   ;  �  �  "  �# A  �$ *  �% X  (( b  �) �   �* �   �+   s, w   �/ 4  �/ =  04 t  n5 1	  �; 	  E   %N g  ,Q �   �V �  cW X  ^ �  ra +  6c �  bg    n '   <n @   dn �   �n    ;o        s  q   �   C  a   �  �        �                   �   �   �   w  7  x       �   �   D      �        [   �       ?      V   �       j   �   R     U  )   .   F   �   �   ]  /  �   �   y  �       �   �  L   \   �       �     �  i  c    K   �               �   �       4   �   {         <                b   �  �   �   M   @   �       �   f  [  H   �     �   b      ^   *           c   �       �   �   �   �           %          �  �       s   �      �       I   �   ~  6   6  �  �   �   M      +  �           +   �   �  g  �                 O                  �   �           0   �           k  �   �          -   >      Y   �       9  R   k   d   r   &  �      |   T       X   5   1      �           �       H  '             
      �   E  `      3  �   ~       w           �   _  h       �   m  �   <   �             �       �   �       d                      8       �      0  �  ?               q  !  }  �   j  e       #  Q   }                 B  n  �      �  ]       	   z   l         �   I    L  �   o   �     �   �   �       �     $   �          �   !   m   n   �   �       C   �      �   �   ^  =   A                �      �   �     �  �   �       r  �   \  {       �   F  8       W  �   N  /       x  W   �   �   �   �  %  �      -  ,   
   i   �          Z  �       e  u              :  A     Z   |          E   9   �       l           u  :   J          &       G   p   �              �   t       �      #             Y  �             �  �   5  (     K      *               �  �     4  "   �   '  J   �   y   $  B   .     �   �   _   �   	           �       Q      �        G         �             ;   �   "      S   �   �   2   �   N   �  �   z      3           �           �      �             V  =  �         1     �   g   �   T  t  �   �   2              �   7       �   �   �      �       f      �   a  �   )  �  p  �   @  ;  P             �  o  �   P  U   >   �  O      v   ,              �          �   �   �  D               �  �   v  h  �   (  �   S  �      `      �   �           X       
    Run a script.  The argument is the module path to a callable.  This
    callable will be imported and then, if --listspec/-l is also given, is
    called with the mailing list as the first argument.  If additional
    arguments are given at the end of the command line, they are passed as
    subsequent positional arguments to the callable.  For additional help, see
    --details.

    If no --listspec/-l argument is given, the script function being called is
    called with no arguments.
     
    Start the named runner, which must be one of the strings returned by the -l
    option.

    For runners that manage a queue directory, optional `slice:range` if given
    is used to assign multiple runner processes to that queue.  range is the
    total number of runners for the queue while slice is the number of this
    runner from [0..range).  For runners that do not manage a queue, slice and
    range are ignored.

    When using the `slice:range` form, you must ensure that each runner for the
    queue is given the same range value.  If `slice:runner` is not given, then
    1:1 is used.
     
- Done. 
- Ignored: 
- Results: 
- Unprocessed: 
Held Messages:
 
Held Subscriptions:
 
Held Unsubscriptions:
 
Replaced multipart/alternative part with first alternative.
 
___________________________________________
Mailman's content filtering has removed the
following MIME parts from this message.
     A more verbose output including the file system paths that Mailman is
    using.     A specification of the mailing list to operate on.  This may be the posting
    address of the list, or its List-ID.  The argument can also be a Python
    regular expression, in which case it is matched against both the posting
    address and List-ID of all mailing lists.  To use a regular expression,
    LISTSPEC must start with a ^ (and the matching is done with re.match().
    LISTSPEC cannot be a regular expression unless --run is given.     Add all member addresses in FILENAME with delivery mode as specified
    with -d/--delivery.  FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
         Add and delete members as necessary to syncronize a list's membership
    with an input file.  FILENAME is the file containing the new membership,
    one member per line.  Blank lines and lines that start with a '#' are
    ignored.  Addresses in FILENAME which are not current list members
    will be added to the list with delivery mode as specified with
    -d/--delivery.  List members whose addresses are not in FILENAME will
    be removed from the list.  FILENAME can be '-' to indicate standard input.
         Add and/or delete owners and moderators of a list.
         Additional metadata key/value pairs to add to the message metadata
    dictionary.  Use the format key=value.  Multiple -m options are
    allowed.     Change a user's email address from old_address to possibly case-preserved
    new_address.
         Configuration file to use.  If not given, the environment variable
    MAILMAN_CONFIG_FILE is consulted and used if set.  If neither are given, a
    default configuration file is loaded.     Create a mailing list.

    The 'fully qualified list name', i.e. the posting address of the mailing
    list is required.  It must be a valid email address and the domain must be
    registered with Mailman.  List names are forced to lower case.     Date: ${date}     Delete all the members of the list.  If specified, none of -f/--file,
    -m/--member or --fromall may be specified.
         Delete list members whose addresses are in FILENAME in addition to those
    specified with -m/--member if any.  FILENAME can be '-' to indicate
    standard input.  Blank lines and lines that start with a '#' are ignored.
         Delete members from a mailing list.     Delete the list member whose address is ADDRESS in addition to those
    specified with -f/--file if any.  This option may be repeated for
    multiple addresses.
         Delete the member(s) specified by -m/--member and/or -f/--file from all
    lists in the installation.  This may not be specified together with
    -a/--all or -l/--list.
         Discard all shunted messages instead of moving them back to their original
    queue.     Display a mailing list's members.
    Filtering along various criteria can be done when displaying.
    With no options given, displaying mailing list members
    to stdout is the default mode.
         Display all memberships for a user or users with address matching a
    pattern. Because part of the process involves converting the pattern
    to a SQL query with wildcards, the pattern should be simple. A simple
    string works best.
         Display only digest members of kind.
    'any' means any digest type, 'plaintext' means only plain text (rfc 1153)
    type digests, 'mime' means MIME type digests.     Display only members with a given ROLE.
    The role may be 'any', 'member', 'nonmember', 'owner', 'moderator',
    or 'administrator' (i.e. owners and moderators).
    If not given, then 'member' role is assumed.     Display only members with a given delivery status.
    'enabled' means all members whose delivery is enabled, 'any' means
    members whose delivery is disabled for any reason, 'byuser' means
    that the member disabled their own delivery, 'bybounces' means that
    delivery was disabled by the automated bounce processor,
    'byadmin' means delivery was disabled by the list
    administrator or moderator, and 'unknown' means that delivery was disabled
    for unknown (legacy) reasons.     Display only memberships with the given role.  If not given, 'all' role,
    i.e. all roles, is the default.     Display only regular delivery members.     Display output to FILENAME instead of stdout.  FILENAME
    can be '-' to indicate standard output.     Don't actually do anything, but in conjunction with --verbose, show what
    would happen.     Don't actually make the changes.  Instead, print out what would be
    done to the list.     Don't attempt to pretty print the object.  This is useful if there is some
    problem with the object and you just want to get an unpickled
    representation.  Useful with 'mailman qfile -i <file>'.  In that case, the
    list of unpickled objects will be left in a variable called 'm'.     Don't print status messages.  Error messages are still printed to standard
    error.     Don't restart the runners when they exit because of an error or a SIGUSR1.
    Use this only for debugging.     Email address of the user to be removed from the given role.
    May be repeated to delete multiple users.
         File to send the output to.  If not given, or if '-' is given, standard
    output is used.     File to send the output to.  If not given, standard output is used.     From: ${from_}     Generate the MTA alias files upon startup. Some MTA, like postfix, can't
    deliver email if alias files mentioned in its configuration are not
    present. In some situations, this could lead to a deadlock at the first
    start of mailman3 server. Setting this option to true will make this
    script create the files and thus allow the MTA to operate smoothly.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option, the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option,the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If you are sure you want to run as root, specify --run-as-root.     Import Mailman 2.1 list data.  Requires the fully-qualified name of the
    list to import and the path to the Mailman 2.1 pickle file.     Increment the digest volume number and reset the digest number to one.  If
    given with --send, the volume number is incremented before any current
    digests are sent.     Key to use for the lookup.  If no section is given, all the key-values pair
    from any section matching the given key will be displayed.     Leaves you at an interactive prompt after all other processing is complete.
    This is the default unless the --run option is given.     List only those mailing lists hosted on the given domain, which
    must be the email host name.  Multiple -d options may be given.
         Master subprocess watcher.

    Start and watch the configured runners, ensuring that they stay alive and
    kicking.  Each runner is forked and exec'd in turn, with the master waiting
    on their process ids.  When it detects a child runner has exited, it may
    restart it.

    The runners respond to SIGINT, SIGTERM, SIGUSR1 and SIGHUP.  SIGINT,
    SIGTERM and SIGUSR1 all cause a runner to exit cleanly.  The master will
    restart runners that have exited due to a SIGUSR1 or some kind of other
    exit condition (say because of an uncaught exception).  SIGHUP causes the
    master and the runners to close their log files, and reopen then upon the
    next printed message.

    The master also responds to SIGINT, SIGTERM, SIGUSR1 and SIGHUP, which it
    simply passes on to the runners.  Note that the master will close and
    reopen its own log files on receipt of a SIGHUP.  The master also leaves
    its own process id in the file specified in the configuration file but you
    normally don't need to use this PID directly.     Message-ID: ${message_id}     Name of file containing the message to inject.  If not given, or
    '-' (without the quotes) standard input is used.     Normally, this script will refuse to run if the user id and group id are
    not set to the 'mailman' user and group (as defined when you configured
    Mailman).  If run as root, this script will change to this user and group
    before the check is made.

    This can be inconvenient for testing and debugging purposes, so the -u flag
    means that the step that sets and checks the uid/gid is skipped, and the
    program is run as the current user and group.  This flag is not recommended
    for normal production environments.

    Note though, that if you run with -u and are not in the mailman group, you
    may have permission problems, such as being unable to delete a list's
    archives through the web.  Tough luck!     Notify the list owner by email that their mailing list has been
    created.     Operate on a mailing list.

    For detailed help, see --details
         Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the digests for all mailing lists.     Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the requests for all mailing lists.     Override the default set of runners that the master will invoke, which is
    typically defined in the configuration file.  Multiple -r options may be
    given.  The values for -r are passed straight through to bin/runner.     Override the list's setting for admin_notify_mchanges.     Override the list's setting for send_goodbye_message to
    deleted members.     Override the list's setting for send_welcome_message to added members.     Override the list's setting for send_welcome_message.     Register the mailing list's domain if not yet registered.  This is
    the default behavior, but these options are provided for backward
    compatibility.  With -D do not register the mailing list's domain.     Run the named runner exactly once through its main loop.  Otherwise, the
    runner runs indefinitely until the process receives a signal.  This is not
    compatible with runners that cannot be run once.     Running mailman commands as root is not recommended and mailman will
    refuse to run as root unless this option is specified.     Section to use for the lookup.  If no key is given, all the key-value pairs
    of the given section will be displayed.     Send any collected digests for the List only if their digest_send_periodic
    is set to True.     Send any collected digests right now, even if the size threshold has not
    yet been met.     Send the added members an invitation rather than immediately adding them.
         Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.     Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.  Ignored for invited
    members.     Set the list's preferred language to CODE, which must be a registered two
    letter language code.     Specify a list owner email address.  If the address is not currently
    registered with Mailman, the address is registered and linked to a user.
    Mailman will send a confirmation message to the address, but it will also
    send a list creation notice to the address.  More than one owner can be
    specified.     Specify a list style name.     Specify the encoding of strings in PICKLE_FILE if not utf-8 or a subset
    thereof. This will normally be the Mailman 2.1 charset of the list's
    preferred_language.     Start a runner.

    The runner named on the command line is started, and it can either run
    through its main loop once (for those runners that support this) or
    continuously.  The latter is how the master runner starts all its
    subprocesses.

    -r is required unless -l or -h is given, and its argument must be one of
    the names displayed by the -l switch.

    Normally, this script should be started from `mailman start`.  Running it
    separately or with -o is generally useful only for debugging.  When run
    this way, the environment variable $MAILMAN_UNDER_MASTER_CONTROL will be
    set which subtly changes some error handling behavior.
         Start an interactive Python session, with a variable called 'm'
    containing the list of unpickled objects.     Subject: ${subject}     The gatenews command is run periodically by the nntp runner.
    If you are running it via cron, you should remove it from the crontab.
    If you want to run it manually, set _MAILMAN_GATENEWS_NNTP in the
    environment.     The list to operate on.  Required unless --fromall is specified.
         The name of the queue to inject the message to.  QUEUE must be one of the
    directories inside the queue directory.  If omitted, the incoming queue is
    used.     The role to add/delete. This may be 'owner' or 'moderator'.
    If not given, then 'owner' role is assumed.
         This option has no effect. It exists for backwards compatibility only.     User to add with the given role. This may be an email address or, if
    quoted, any display name and email address parseable by
    email.utils.parseaddr. E.g., 'Jane Doe <jane@example.com>'. May be repeated
    to add multiple users.
         [MODE] Add all member addresses in FILENAME.  This option is removed.
    Use 'mailman addmembers' instead.     [MODE] Delete all member addresses found in FILENAME.
    This option is removed. Use 'mailman delmembers' instead.     [MODE] Synchronize all member addresses of the specified mailing list
    with the member addresses found in FILENAME.
    This option is removed. Use 'mailman syncmembers' instead.  (Digest mode) ${count} matching mailing lists found: ${display_name} post acknowledgment ${email} is already a ${role.name} of ${mlist.fqdn_listname} ${email} is not a ${role.name} of ${mlist.fqdn_listname} ${member} unsubscribed from ${mlist.display_name} mailing list due to bounces ${member}'s bounce score incremented on ${mlist.display_name} ${member}'s subscription disabled on ${mlist.display_name} ${mlist.display_name} Digest, Vol ${volume}, Issue ${digest_number} ${mlist.display_name} mailing list probe message ${mlist.display_name} subscription notification ${mlist.display_name} unsubscription notification ${mlist.fqdn_listname} post from ${msg.sender} requires approval ${mlist.list_id} bumped to volume ${mlist.volume}, number ${mlist.next_digest_number} ${mlist.list_id} has no members ${mlist.list_id} is at volume ${mlist.volume}, number ${mlist.next_digest_number} ${mlist.list_id} sent volume ${mlist.volume}, number ${mlist.next_digest_number} ${name} runs ${classname} ${person} has a pending subscription for ${listname} ${person} left ${mlist.fqdn_listname} ${realname} via ${mlist.display_name} ${self.email} is already a member of mailing list ${self.fqdn_listname} ${self.email} is already a moderator of mailing list ${self.fqdn_listname} ${self.email} is already a non-member of mailing list ${self.fqdn_listname} ${self.email} is already an owner of mailing list ${self.fqdn_listname} ${self.name}: ${email} is not a member of ${mlist.fqdn_listname} ${self.name}: No valid address found to subscribe ${self.name}: No valid email address found to unsubscribe ${self.name}: no such command: ${command_name} ${self.name}: too many arguments: ${printable_arguments} (no subject) - Original message details: -------------- next part --------------
 --send and --periodic flags cannot be used together <----- start object ${count} -----> A message part incompatible with plain text digests has been removed ...
Name: ${filename}
Type: ${ctype}
Size: ${size} bytes
Desc: ${desc}
 A previous run of GNU Mailman did not exit cleanly ({}).  Try using --force A rule which always matches. Accept a message. Add a list-specific prefix to the Subject header value. Add the RFC 2369 List-* headers. Add the message to the archives. Add the message to the digest, possibly sending it. Address changed from {} to {}. Address {} already exists; can't change. Address {} is not a valid email address. Address {} not found. Addresses are not different.  Nothing to change. After content filtering, the message was empty Already subscribed (skipping): ${email} An alias for 'end'. An alias for 'join'. An alias for 'leave'. An alternative directory to output the various MTA files to. And you can print the list's request address by running:

% mailman withlist -r listaddr.requestaddr -l ant@example.com
Importing listaddr ...
Running listaddr.requestaddr() ...
ant-request@example.com Announce only mailing list style. Apply DMARC mitigations. As another example, say you wanted to change the display name for a particular
mailing list.  You could put the following function in a file called
`change.py`:

def change(mlist, display_name):
    mlist.display_name = display_name

and run this from the command line:

% mailman withlist -r change -l ant@example.com 'My List'

Note that you do not have to explicitly commit any database transactions, as
Mailman will do this for you (assuming no errors occured). Auto-response for your message to the "${display_name}" mailing list Bad runner spec: ${value} Calculate the owner and moderator recipients. Calculate the regular recipients of the message. Cannot import runner module: ${class_path} Cannot parse as valid email address (skipping): ${line} Cannot unshunt message ${filebase}, skipping:
${error} Catch messages that are bigger than a specified maximum. Catch messages with digest Subject or boilerplate quote. Catch messages with implicit destination. Catch messages with no, or empty, Subject headers. Catch messages with suspicious headers. Catch messages with too many explicit recipients. Catch mis-addressed email commands. Cleanse certain headers from all messages. Confirm a subscription or held message request. Confirmation email sent to ${person} Confirmation email sent to ${person} to leave ${mlist.fqdn_listname} Confirmation token did not match Confirmed Content filter message notification Created mailing list: ${mlist.fqdn_listname} DMARC moderation Decorate a message with headers and footers. Digest Footer Digest Header Discard a message and stop processing. Discussion mailing list style with private archives. Display Mailman's version. Display more debugging information to the log file. Echo back your arguments. Email: {} Emergency moderation is in effect for this list End of  Filter the MIME content of messages. Find DMARC policy of From: domain. For unknown reasons, the master lock could not be acquired.

Lock file: ${config.LOCK_FILE}
Lock host: ${hostname}

Exiting. Forward of moderated message GNU Mailman is already running GNU Mailman is in an unexpected state (${hostname} != ${fqdn_name}) GNU Mailman is not running GNU Mailman is running (master pid: ${pid}) GNU Mailman is stopped (stale pid: ${pid}) Generating MTA alias maps Get a list of the list members. Get help about available email commands. Get information out of a queue file. Get the normal delivery recipients from an include file. Header "{}" matched a bounce_matching_header line Header "{}" matched a header rule Here's an example of how to use the --run option.  Say you have a file in the
Mailman installation directory called 'listaddr.py', with the following two
functions:

def listaddr(mlist):
    print(mlist.posting_address)

def requestaddr(mlist):
    print(mlist.request_address)

Run methods take at least one argument, the mailing list object to operate
on.  Any additional arguments given on the command line are passed as
positional arguments to the callable.

If -l is not given then you can run a function that takes no arguments.
 Hold a message and stop processing. If you reply to this message, keeping the Subject: header intact, Mailman will
discard the held message.  Do this if the message is spam.  If you reply to
this message and include an Approved: header with the list password in it, the
message will be approved for posting to the list.  The Approved: header can
also appear in the first line of the body of the reply. Ignoring non-dictionary: {0!r} Ignoring non-text/plain MIME parts Illegal list name: ${fqdn_listname} Illegal owner addresses: ${invalid} Information about this Mailman instance. Inject a message from a file into a mailing list's queue. Invalid Approved: password Invalid email address: ${email} Invalid language code: ${language_code} Invalid or unverified email address: ${email} Invalid value for [shell]use_python: {} Is the master even running? Join this mailing list. Last autoresponse notification for today Leave this mailing list. Leave this mailing list.

You may be asked to confirm your request. Less verbosity List all mailing lists. List already exists: ${fqdn_listname} List only those mailing lists that are publicly advertised List the available runner names and exit. List: {} Look for a posting loop. Look for any previous rule hit. Match all messages posted to a mailing list that gateways to a
        moderated newsgroup.
         Match messages sent by banned addresses. Match messages sent by moderated members. Match messages sent by nonmembers. Match messages with no valid senders. Member not subscribed (skipping): ${email} Members of the {} mailing list:
{} Membership is banned (skipping): ${email} Message ${message} Message contains administrivia Message has a digest subject Message has already been posted to this list Message has implicit destination Message has more than {} recipients Message has no subject Message quotes digest boilerplate Message sender {} is banned from this list Missing data for request {}

 Moderation chain Modify message headers. Move the message to the outgoing news queue. N/A Name: ${fname}
 New subscription request to ${self.mlist.display_name} from ${self.address.email} New unsubscription request from ${mlist.display_name} by ${email} New unsubscription request to ${self.mlist.display_name} from ${self.address.email} No child with pid: ${pid} No confirmation token found No matching mailing lists found No registered user for email address: ${email} No runner name given. No sender was found in the message. No such command: ${command_name} No such list found: ${spec} No such list matching spec: ${listspec} No such list: ${_list} No such list: ${listspec} No such queue: ${queue} Not a Mailman 2.1 configuration file: ${pickle_file} Nothing to add or delete. Nothing to do Notify list owners/moderators of pending requests. Number of objects found (see the variable 'm'): ${count} Operate on digests. Ordinary discussion mailing list style. Original Message PID unreadable in: ${config.PID_FILE} Perform ARC auth checks and attach resulting headers Perform auth checks and attach Authentication-Results header. Perform some bookkeeping after a successful post. Poll the NNTP server for messages to be gatewayed to mailing lists. Post to a moderated newsgroup gateway Posting of your message titled "${subject}" Print detailed instructions and exit. Print less output. Print some additional status. Print the Mailman configuration. Process DMARC reject or discard mitigations Produces a list of member names and email addresses.

The optional delivery= and mode= arguments can be used to limit the report
to those members with matching delivery status and/or delivery mode.  If
either delivery= or mode= is specified more than once, only the last occurrence
is used.
 Programmatically, you can write a function to operate on a mailing list, and
this script will take care of the housekeeping (see below for examples).  In
that case, the general usage syntax is:

% mailman withlist [options] -l listspec [args ...]

where `listspec` is either the posting address of the mailing list
(e.g. ant@example.com), or the List-ID (e.g. ant.example.com). Reason: {}

 Regenerate the aliases appropriate for your MTA. Regular expression requires --run Reject/bounce a message and stop processing. Remove DomainKeys headers. Remove a mailing list. Removed list: ${listspec} Reopening the Mailman runners Request to mailing list "${display_name}" rejected Restarting the Mailman runners Send an acknowledgment of a posting. Send automatic responses. Send the message to the outgoing queue. Sender: {}
 Show a list of all available queue names and exit. Show also the list descriptions Show also the list names Show the current running status of the Mailman system. Show this help message and exit. Shutting down Mailman's master runner Signal the Mailman processes to re-open their log files. Stale pid file removed. Start the Mailman master and runner processes. Starting Mailman's master runner Stop and restart the Mailman runner subprocesses. Stop processing commands. Stop the Mailman master and runner processes. Subject: {}
 Subscription already pending (skipping): ${email} Subscription request Suppress some duplicates of the same message. Suppress status messages Tag messages with topic matches. The Mailman Replybot The attached message matched the ${mlist.display_name} mailing list's content
filtering rules and was prevented from being forwarded on to the list
membership.  You are receiving the only remaining copy of the discarded
message.

 The built-in -owner posting chain. The built-in header matching chain The built-in moderation chain. The built-in owner pipeline. The built-in posting pipeline. The content of this message was lost. It was probably cross-posted to
multiple lists and previously handled on another list.
 The mailing list is in emergency hold and this message was not
        pre-approved by the list administrator.
         The master lock could not be acquired because it appears as though another
master is already running. The master lock could not be acquired, because it appears as if some process
on some other host may have acquired it.  We can't test for stale locks across
host boundaries, so you'll have to clean this up manually.

Lock file: ${config.LOCK_FILE}
Lock host: ${hostname}

Exiting. The master lock could not be acquired.  It appears as though there is a stale
master lock.  Try re-running ${program} with the --force flag. The message comes from a moderated member The message has a matching Approve or Approved header. The message has no valid senders The message is larger than the {} KB maximum size The message is not from a list member The message's content type was explicitly disallowed The message's content type was not explicitly allowed The message's file extension was explicitly disallowed The message's file extension was not explicitly allowed The results of your email command are provided below.
 The results of your email commands The sender is in the nonmember {} list The variable 'm' is the ${listspec} mailing list The virgin queue pipeline. The {} list has {} moderation requests waiting. There are two ways to use this script: interactively or programmatically.
Using it interactively allows you to play with, examine and modify a mailing
list from Python's interactive interpreter.  When running interactively, the
variable 'm' will be available in the global namespace.  It will reference the
mailing list object. This script provides you with a general framework for interacting with a
mailing list. Today's Topics (${count} messages) Today's Topics:
 Uncaught bounce notification Undefined domain: ${domain} Undefined runner name: ${name} Unknown list style name: ${style_name} Unrecognized or invalid argument(s):
{} Unshunt messages. Unsubscription request User: {}
 Welcome to the "${mlist.display_name}" mailing list${digmode} You are not allowed to post to this mailing list From: a domain which publishes a DMARC policy of reject or quarantine, and your message has been automatically rejected.  If you think that your messages are being rejected in error, contact the mailing list owner at ${listowner}. You are not authorized to see the membership list. You can print the list's posting address by running the following from the
command line:

% mailman withlist -r listaddr -l ant@example.com
Importing listaddr ...
Running listaddr.listaddr() ...
ant@example.com You have been invited to join the ${event.mlist.fqdn_listname} mailing list. You have been unsubscribed from the ${mlist.display_name} mailing list You will be asked to confirm your subscription request and you may be issued a
provisional password.

By using the 'digest' option, you can specify whether you want digest delivery
or not.  If not specified, the mailing list's default delivery mode will be
used.  You can use the 'address' option to request subscription of an address
other than the sender of the command.
 Your confirmation is needed to join the ${event.mlist.fqdn_listname} mailing list. Your confirmation is needed to leave the ${event.mlist.fqdn_listname} mailing list. Your message to ${mlist.fqdn_listname} awaits moderator approval Your new mailing list: ${fqdn_listname} Your subscription for ${mlist.display_name} mailing list has been disabled Your urgent message to the ${mlist.display_name} mailing list was not
authorized for delivery.  The original message as received by Mailman is
attached.
 [${mlist.display_name}]  [----- end pickle -----] [----- start pickle -----] [ADD] %s [DEL] %s [No bounce details are available] [No details are available] [No reason given] [No reasons given] bad argument: ${argument} domain:admin:notice:new-list.txt help.txt ipython is not available, set use_ipython to no list:admin:action:post.txt list:admin:action:subscribe.txt list:admin:action:unsubscribe.txt list:admin:notice:disable.txt list:admin:notice:increment.txt list:admin:notice:pending.txt list:admin:notice:removal.txt list:admin:notice:subscribe.txt list:admin:notice:unrecognized.txt list:admin:notice:unsubscribe.txt list:member:digest:masthead.txt list:member:generic:footer.txt list:user:action:invite.txt list:user:action:subscribe.txt list:user:action:unsubscribe.txt list:user:notice:hold.txt list:user:notice:no-more-today.txt list:user:notice:post.txt list:user:notice:probe.txt list:user:notice:refuse.txt list:user:notice:rejected.txt list:user:notice:warning.txt list:user:notice:welcome.txt n/a not available readline not available slice and range must be integers: ${value} {} Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-12-30 04:00+0000
Last-Translator: தமிழ்நேரம் <anishprabu.t@gmail.com>
Language-Team: Tamil <https://hosted.weblate.org/projects/gnu-mailman/mailman/ta/>
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.10-dev
 
    ச்கிரிப்டை இயக்கவும். உரையாடல் என்பது அழைக்கக்கூடிய தொகுதி பாதை. இது
 அழைக்கக்கூடியது இறக்குமதி செய்யப்படும், பின்னர், --listspec/-l கொடுக்கப்பட்டால்,
 முதல் வாதமாக அஞ்சல் பட்டியலுடன் அழைக்கப்படுகிறது. கூடுதல் என்றால்
 கட்டளை வரியின் முடிவில் வாதங்கள் வழங்கப்படுகின்றன, அவை அனுப்பப்படுகின்றன
 அழைக்கக்கூடிய அடுத்தடுத்த நிலை வாதங்கள். கூடுதல் உதவிக்கு, பார்க்க
 -இலக்கு.

 இல்லை-listspec/-l உரையாடல் வழங்கப்பட்டால், ச்கிரிப்ட் செயல்பாடு அழைக்கப்படுகிறது
 எந்த வாதங்களும் இல்லாமல் அழைக்கப்படுகிறது.
     
    பெயரிடப்பட்ட ரன்னரைத் தொடங்குங்கள், இது -l ஆல் திரும்பிய சரங்களில் ஒன்றாக இருக்க வேண்டும்
 விருப்பம்.

 வரிசை கோப்பகத்தை நிர்வகிக்கும் ஓட்டப்பந்தய வீரர்களுக்கு, விருப்பமான `துண்டு: வரம்பு` கொடுக்கப்பட்டால்
 அந்த வரிசையில் பல ரன்னர் செயல்முறைகளை ஒதுக்க பயன்படுகிறது. வரம்பு
 வரிசையில் வரிசைக்கான மொத்த ஓட்டப்பந்தய வீரர்களின் எண்ணிக்கை இதன் எண்ணிக்கை
 [0..Range) இலிருந்து ரன்னர். ஒரு வரிசையை நிர்வகிக்காத ஓட்டப்பந்தய வீரர்களுக்கு, துண்டு மற்றும்
 வரம்பு புறக்கணிக்கப்படுகிறது.

 `ச்லைச்: ரேஞ்ச்` படிவத்தைப் பயன்படுத்தும் போது, ஒவ்வொரு ஓட்டப்பந்தய வீரரையும் நீங்கள் உறுதிப்படுத்த வேண்டும்
 வரிசையில் அதே வரம்பு மதிப்பு வழங்கப்படுகிறது. `துண்டு: ரன்னர்` வழங்கப்படவில்லை என்றால், பின்னர்
 1: 1 பயன்படுத்தப்படுகிறது.
     
- முடிந்தது. 
- புறக்கணிக்கப்பட்டது: 
- முடிவுகள்: 
- பதப்படுத்தப்படாத: 
வைத்திருக்கும் செய்திகள்:
 
வைத்திருந்த சந்தாக்கள்:
 
குழுவிலகங்கள்:
 
மல்டிபார்ட்/மாற்று பகுதியை முதல் மாற்றுடன் மாற்றியது.
 
___________________________________________
 மெயில்மேனின் உள்ளடக்க வடிகட்டுதல் அகற்றப்பட்டுள்ளது
 இந்த செய்தியிலிருந்து மைம் பகுதிகளைப் பின்பற்றுகிறது.
     மெயில்மேன் என்ற கோப்பு முறைமை பாதைகள் உட்பட மிகவும் சொற்களஞ்சிய வெளியீடு
 பயன்படுத்துகிறது.     செயல்பட அஞ்சல் பட்டியலின் விவரக்குறிப்பு. இது இடுகையாக இருக்கலாம்
 பட்டியலின் முகவரி, அல்லது அதன் பட்டியல்-ஐடி. உரையாடல் ஒரு மலைப்பாம்பாகவும் இருக்கலாம்
 வழக்கமான வெளிப்பாடு, இந்த விசயத்தில் இது இடுகை இரண்டிற்கும் பொருந்துகிறது
 அனைத்து அஞ்சல் பட்டியல்களின் முகவரி மற்றும் பட்டியல்-ஐடி. வழக்கமான வெளிப்பாட்டைப் பயன்படுத்த,
 ListSpec A ^ உடன் தொடங்க வேண்டும் (மேலும் பொருந்தக்கூடியது Re.Match () உடன் செய்யப்படுகிறது.
 -ரன் வழங்கப்படாவிட்டால் லிச்ட்ச்பெக் ஒரு வழக்கமான வெளிப்பாடாக இருக்க முடியாது.     குறிப்பிட்டபடி அனைத்து உறுப்பினர் முகவரியுகளையும் கோப்பு பெயரில் சேர்க்கவும்
 -d/-விநியோகத்துடன். நிலையான உள்ளீட்டைக் குறிக்க கோப்பு பெயர் '-' ஆக இருக்கலாம்.
 '#' உடன் தொடங்கும் வெற்று கோடுகள் மற்றும் கோடுகள் புறக்கணிக்கப்படுகின்றன.
         ஒரு பட்டியலின் உறுப்பினர்களை ஒத்திசைக்க தேவையான உறுப்பினர்களைச் சேர்த்து நீக்கவும்
 உள்ளீட்டு கோப்புடன். கோப்பு பெயர் புதிய உறுப்பினர் கொண்ட கோப்பு,
 ஒரு வரிக்கு ஒரு உறுப்பினர். '#' உடன் தொடங்கும் வெற்று கோடுகள் மற்றும் கோடுகள்
 புறக்கணிக்கப்பட்டது. தற்போதைய பட்டியல் உறுப்பினர்கள் இல்லாத கோப்பு பெயரில் உள்ள முகவரிகள்
 குறிப்பிடப்பட்டுள்ளபடி டெலிவரி பயன்முறையுடன் பட்டியலில் சேர்க்கப்படும்
 -d/-டெலிவரி. கோப்பு பெயரில் இல்லாத முகவரிகள் அல்ல
 பட்டியலிலிருந்து அகற்றப்படும். நிலையான உள்ளீட்டைக் குறிக்க கோப்பு பெயர் '-' ஆக இருக்கலாம்.
         ஒரு பட்டியலின் உரிமையாளர்கள் மற்றும் மதிப்பீட்டாளர்களைச் சேர்க்கவும்/அல்லது நீக்கவும்.
         செய்தி மெட்டாடேட்டாவில் சேர்க்க கூடுதல் மேனிலை தரவு விசை/மதிப்பு சோடிகள்
 அகராதி. வடிவமைப்பு விசை = மதிப்பைப் பயன்படுத்தவும். பல -எம் விருப்பங்கள்
 அனுமதிக்கப்படுகிறது.     ஒரு பயனரின் மின்னஞ்சல் முகவரியை பழைய_அடிரெச்சிலிருந்து வழக்கு-பாதுகாக்கப்பட்டதாக மாற்றவும்
 new_address.
         பயன்படுத்த உள்ளமைவு கோப்பு. வழங்கப்படாவிட்டால், சூழல் மாறுபடும்
 Mailman_config_file ஆலோசப்பட்டு அமைக்கப்பட்டால் பயன்படுத்தப்படுகிறது. இரண்டுமே கொடுக்கப்படவில்லை என்றால், அ
 இயல்புநிலை உள்ளமைவு கோப்பு ஏற்றப்படுகிறது.     ஒரு அஞ்சல் பட்டியலை உருவாக்கவும்.

 'முழு தகுதி வாய்ந்த பட்டியல் பெயர்', அதாவது அஞ்சலின் இடுகையிடும் முகவரி
 பட்டியல் தேவை. இது செல்லுபடியாகும் மின்னஞ்சல் முகவரியாக இருக்க வேண்டும் மற்றும் டொமைன் இருக்க வேண்டும்
 மெயில்மேனுடன் பதிவு செய்யப்பட்டுள்ளது. பட்டியல் பெயர்கள் சிறிய வழக்குக்கு கட்டாயப்படுத்தப்படுகின்றன.     தேதி: $ {date}     பட்டியலின் அனைத்து உறுப்பினர்களையும் நீக்கவும். குறிப்பிடப்பட்டால், -f/-கோப்பு எதுவும் இல்லை,
 -m/-உறுப்பினர் அல்லது --fromall குறிப்பிடப்படலாம்.
         பட்டியல் உறுப்பினர்களை நீக்கு
 -m/-உறுப்பினர் ஏதேனும் இருந்தால் குறிப்பிடப்பட்டுள்ளது. கோப்பு பெயர் '-' ஆக இருக்கலாம்
 நிலையான உள்ளீடு. '#' உடன் தொடங்கும் வெற்று கோடுகள் மற்றும் கோடுகள் புறக்கணிக்கப்படுகின்றன.
         ஒரு அஞ்சல் பட்டியலிலிருந்து உறுப்பினர்களை நீக்கு.     பட்டியல் உறுப்பினரை நீக்கு
 -f/-கோப்பு ஏதேனும் இருந்தால் குறிப்பிடப்பட்டுள்ளது. இந்த விருப்பம் மீண்டும் மீண்டும் செய்யப்படலாம்
 பல முகவரிகள்.
         அனைவரிடமிருந்தும் -m/-உறுப்பினர் மற்றும்/அல்லது -f/-கோப்பால் குறிப்பிடப்பட்ட உறுப்பினர் (களை) நீக்கு
 நிறுவலில் பட்டியல்கள். இது ஒன்றாக குறிப்பிடப்படாமல் இருக்கலாம்
 -a/-அனைத்தும் அல்லது -l/-பட்டியல்.
         சன்ட் செய்யப்பட்ட அனைத்து செய்திகளையும் அவற்றின் அசல் நிலைக்கு நகர்த்துவதற்குப் பதிலாக அவற்றை நிராகரிக்கவும்
 வரிசை.     அஞ்சல் பட்டியல் உறுப்பினர்களைக் காண்பி. காண்பிக்கும் போது பல்வேறு அளவுகோல்களுடன் வடிகட்டலாம். எந்த விருப்பங்களும் கொடுக்கப்படாமல், அஞ்சல் பட்டியல் உறுப்பினர்களை stdout க்கு காண்பிப்பது இயல்புநிலை பயன்முறையாகும்.
         முகவரி பொருந்தக்கூடிய ஒரு பயனர் அல்லது பயனர்களுக்கான அனைத்து உறுப்பினர்களையும் காண்பி
 முறை. ஏனெனில் செயல்முறையின் ஒரு பகுதி வடிவத்தை மாற்றுவதை உள்ளடக்குகிறது
 வைல்டு கார்டுகளுடன் ஒரு கவிமொ வினவலுக்கு, முறை எளிமையாக இருக்க வேண்டும். ஒரு எளிய
 சரம் சிறப்பாக செயல்படுகிறது.
         வகையான உறுப்பினர்களை மட்டுமே டைசச்ட் செய்யுங்கள்.
 'ஏதேனும்' என்பது எந்த டைசச்ட் வகையையும் குறிக்கிறது, 'எளிய உரை' என்பது எளிய உரை மட்டுமே (RFC 1153)
 செரிமானங்களைத் தட்டச்சு செய்க, 'மைம்' என்றால் மைம் வகை செரிமானங்கள்.     கொடுக்கப்பட்ட பாத்திரத்துடன் உறுப்பினர்களை மட்டுமே காண்பி.
 பாத்திரம் 'ஏதேனும்', 'உறுப்பினர்', 'அல்லாதவர்', 'உரிமையாளர்', 'மதிப்பீட்டாளர்',
 அல்லது 'நிர்வாகி' (அதாவது உரிமையாளர்கள் மற்றும் மதிப்பீட்டாளர்கள்).
 கொடுக்கப்படாவிட்டால், 'உறுப்பினர்' பங்கு கருதப்படுகிறது.     கொடுக்கப்பட்ட விநியோக நிலையுடன் உறுப்பினர்களை மட்டுமே காண்பி.
 'இயக்கப்பட்டது' என்பது மகப்பேறு இயக்கப்பட்ட அனைத்து உறுப்பினர்களையும் குறிக்கிறது, 'ஏதேனும்' பொருள்
 எந்தவொரு காரணத்திற்காகவும் வழங்கப்படும் உறுப்பினர்கள், 'BYUSER' என்றால்
 உறுப்பினர் தங்கள் சொந்த விநியோகத்தை முடக்கியுள்ளார், 'பைபவுன்ச்' என்றால் அது
 தானியங்கு பவுன்ச் செயலியால் டெலிவரி முடக்கப்பட்டது,
 'பைட்மின்' என்றால் பட்டியலால் டெலிவரி முடக்கப்பட்டது
 நிர்வாகி அல்லது மதிப்பீட்டாளர், மற்றும் 'அறியப்படாத' என்றால் டெலிவரி முடக்கப்பட்டது
 அறியப்படாத (மரபு) காரணங்களுக்காக.     கொடுக்கப்பட்ட பாத்திரத்துடன் உறுப்பினர்களை மட்டுமே காண்பி. கொடுக்கப்படாவிட்டால், 'அனைத்தும்' பாத்திரம்,
 அதாவது அனைத்து பாத்திரங்களும் இயல்புநிலை.     வழக்கமான விநியோக உறுப்பினர்களை மட்டுமே காண்பி.     Stdout க்கு பதிலாக கோப்பு பெயருக்கு வெளியீட்டைக் காண்பி. கோப்பு பெயர்
 நிலையான வெளியீட்டைக் குறிக்க '-' ஆக இருக்கலாம்.     உண்மையில் எதுவும் செய்ய வேண்டாம், ஆனால் --verbose உடன் இணைந்து, என்ன காட்டுங்கள்
 நடக்கும்.     உண்மையில் மாற்றங்களைச் செய்ய வேண்டாம். அதற்கு பதிலாக, என்னவாக இருக்கும் என்பதை அச்சிடுங்கள்
 பட்டியலில் முடிந்தது.     பொருளை அழகாக அச்சிட முயற்சிக்காதீர்கள். சில இருந்தால் இது பயனுள்ளதாக இருக்கும்
 பொருளின் சிக்கல் மற்றும் நீங்கள் ஒரு திறக்கப்பட வேண்டும்
 பிரதிநிதித்துவம். 'மெயில்மேன் qfile -i <file>' உடன் பயனுள்ளதாக இருக்கும். அந்த விசயத்தில், தி
 திறக்கப்படாத பொருட்களின் பட்டியல் 'எம்' எனப்படும் மாறியில் விடப்படும்.     நிலை செய்திகளை அச்சிட வேண்டாம். பிழை செய்திகள் இன்னும் தரத்திற்கு அச்சிடப்பட்டுள்ளன
 பிழை.     பிழை அல்லது SIGUSR1 காரணமாக வெளியேறும்போது ஓட்டப்பந்தய வீரர்கள் மறுதொடக்கம் செய்ய வேண்டாம்.
 பிழைத்திருத்தத்திற்கு மட்டுமே இதைப் பயன்படுத்தவும்.     கொடுக்கப்பட்ட பாத்திரத்திலிருந்து அகற்றப்பட வேண்டிய பயனரின் மின்னஞ்சல் முகவரி.
 பல பயனர்களை நீக்க மீண்டும் மீண்டும் செய்யலாம்.
         வெளியீட்டை அனுப்ப கோப்பு. கொடுக்கப்படாவிட்டால், அல்லது '-' வழங்கப்பட்டால், தரநிலை
 வெளியீடு பயன்படுத்தப்படுகிறது.     வெளியீட்டை அனுப்ப கோப்பு. கொடுக்கப்படாவிட்டால், நிலையான வெளியீடு பயன்படுத்தப்படுகிறது.     இருந்து: $ {from_}     தொடக்கத்தில் MTA மாற்றுப்பெயர் கோப்புகளை உருவாக்குங்கள். போச்ட்ஃபிக்ச் போன்ற சில எம்.டி.ஏ முடியாது
 அதன் உள்ளமைவில் குறிப்பிடப்பட்டுள்ள மாற்றுப்பெயர் கோப்புகள் இல்லை என்றால் மின்னஞ்சலை வழங்கவும்
 தற்போது. சில சூழ்நிலைகளில், இது முதலில் ஒரு முட்டுக்கட்டைக்கு வழிவகுக்கும்
 மெயில்மேன் 3 சேவையகத்தின் தொடக்க. இந்த விருப்பத்தை உண்மைக்கு அமைப்பது இதை உருவாக்கும்
 ச்கிரிப்ட் கோப்புகளை உருவாக்கி, இதனால் எம்.டி.ஏ சீராக செயல்பட அனுமதிக்கிறது.     மாச்டர் வாட்சர் ஏற்கனவே இருக்கும் மாச்டர் பூட்டைக் கண்டால், அது பொதுவாக வெளியேறும்
 பிழை செய்தியுடன். இந்த விருப்பத்துடன், மாச்டர் கூடுதல் செய்வார்
 சோதனை நிலை. விவரிக்கப்பட்டுள்ள ஓச்ட்/பிஐடியுடன் பொருந்தக்கூடிய ஒரு செயல்முறை என்றால்
 பூட்டு கோப்பு இயங்குகிறது, மாச்டர் இன்னும் வெளியேறுவார், உங்களுக்கு கைமுறையாக தேவைப்படுகிறது
 பூட்டை தூய்மை செய்யுங்கள். ஆனால் பொருந்தக்கூடிய செயல்முறை எதுவும் கிடைக்கவில்லை என்றால், மாச்டர் செய்வார்
 வெளிப்படையான பழமையான பூட்டை அகற்றி, உரிமை கோர மற்றொரு முயற்சியை மேற்கொள்ளுங்கள்
 முதன்மை பூட்டு.     மாச்டர் வாட்சர் ஏற்கனவே இருக்கும் மாச்டர் பூட்டைக் கண்டால், அது பொதுவாக வெளியேறும்
 பிழை செய்தியுடன். இந்த விருப்பத்துடன், மாச்டர் கூடுதல் செய்வார்
 சோதனை நிலை. விவரிக்கப்பட்டுள்ள ஓச்ட்/பிஐடியுடன் பொருந்தக்கூடிய ஒரு செயல்முறை என்றால்
 பூட்டு கோப்பு இயங்குகிறது, மாச்டர் இன்னும் வெளியேறுவார், உங்களுக்கு கைமுறையாக தேவைப்படுகிறது
 பூட்டை தூய்மை செய்யுங்கள். ஆனால் பொருந்தக்கூடிய செயல்முறை எதுவும் கிடைக்கவில்லை என்றால், மாச்டர் செய்வார்
 வெளிப்படையான பழமையான பூட்டை அகற்றி, உரிமை கோர மற்றொரு முயற்சியை மேற்கொள்ளுங்கள்
 முதன்மை பூட்டு.     நீங்கள் ரூட்டாக இயக்க விரும்புகிறீர்கள் என்பது உறுதி என்றால்,-ரன்-ரூட் குறிப்பிடவும்.     மெயில்மேன் 2.1 பட்டியல் தரவை இறக்குமதி செய்யுங்கள். முழு தகுதி வாய்ந்த பெயர் தேவை
 இறக்குமதி செய்ய பட்டியல் மற்றும் மெயில்மேன் 2.1 ஊறுகாய் கோப்புக்கான பாதை.     டைசச்ட் தொகுதி எண்ணை அதிகரிக்கவும், டைசச்ட் எண்ணை ஒன்றுக்கு மீட்டமைக்கவும். என்றால்
 - -அனுப்பப்பட்டால், எந்த மின்னோட்டத்திற்கும் முன் தொகுதி எண் அதிகரிக்கப்படுகிறது
 செரிமானங்கள் அனுப்பப்படுகின்றன.     தேடலுக்கு பயன்படுத்த விசை. எந்த பகுதியும் கொடுக்கப்படவில்லை என்றால், அனைத்து முக்கிய மதிப்புகளும் இணை
 கொடுக்கப்பட்ட விசையுடன் பொருந்தக்கூடிய எந்த பிரிவிலிருந்தும் காண்பிக்கப்படும்.     மற்ற எல்லா செயலாக்கமும் முடிந்ததும் உங்களை ஒரு ஊடாடும் வரியில் விட்டுவிடுகிறது.
 -ரன் விருப்பம் வழங்கப்படாவிட்டால் இது இயல்புநிலை.     கொடுக்கப்பட்ட டொமைனில் புரவலன் செய்யப்பட்ட அந்த அஞ்சல் பட்டியல்களை மட்டுமே பட்டியலிடுங்கள், இது
 மின்னஞ்சல் புரவலன் பெயராக இருக்க வேண்டும். பல -டி விருப்பங்கள் வழங்கப்படலாம்.
         மாச்டர் சப்ரோசெச் வாட்சர்.

 கட்டமைக்கப்பட்ட ஓட்டப்பந்தய வீரர்களைத் தொடங்கி பாருங்கள், அவர்கள் உயிருடன் இருப்பதை உறுதிசெய்கிறார்கள்
 உதைத்தல். ஒவ்வொரு ஓட்டப்பந்தய வீரரும் முட்கரண்டி மற்றும் மாச்டர் காத்திருப்பதன் மூலம் செயல்படுத்தப்படுகிறார்
 அவற்றின் செயல்முறை ஐடிகளில். இது ஒரு குழந்தை ஓட்டப்பந்தய வீரர் வெளியேறுவதைக் கண்டறிந்தால், அது இருக்கலாம்
 அதை மறுதொடக்கம் செய்யுங்கள்.

 ஓட்டப்பந்தய வீரர்கள் SIGINT, SIGTERM, SIGUSR1 மற்றும் SIGUP க்கு பதிலளிக்கின்றனர். Sigint,
 Sigterm மற்றும் Sigusr1 அனைத்தும் ஒரு ரன்னர் சுத்தமாக வெளியேற காரணமாகின்றன. மாச்டர் வில்
 ஒரு SIGUSR1 அல்லது சில வகையான காரணமாக வெளியேறும் ஓட்டப்பந்தய வீரர்களை மறுதொடக்கம் செய்யுங்கள்
 வெளியேறும் நிபந்தனை (கவனிக்கப்படாத விதிவிலக்கு காரணமாக சொல்லுங்கள்). CIGHUP ஏற்படுகிறது
 மாச்டர் மற்றும் ரன்னர்கள் தங்கள் பதிவுக் கோப்புகளை மூட, பின்னர் மீண்டும் திறக்கவும்
 அடுத்த அச்சிடப்பட்ட செய்தி.

 மாச்டர் SIGINT, SIGTERM, Sigusr1 மற்றும் Sighup க்கும் பதிலளிக்கிறார், அது
 வெறுமனே ஓட்டப்பந்தய வீரர்களுக்கு செல்கிறது. மாச்டர் மூடுவார் என்பதை நினைவில் கொள்க
 ஒரு அளவிலான ரசீதில் அதன் சொந்த பதிவு கோப்புகளை மீண்டும் திறக்கவும். மாச்டர் வெளியேறுகிறார்
 உள்ளமைவு கோப்பில் குறிப்பிடப்பட்டுள்ள கோப்பில் அதன் சொந்த செயல்முறை ஐடி ஆனால் நீங்கள்
 பொதுவாக இந்த பிஐடியை நேரடியாகப் பயன்படுத்த தேவையில்லை.     செய்தி-ஐடி: $ {message_id}     செலுத்த செய்தியைக் கொண்ட கோப்பின் பெயர். கொடுக்கப்படவில்லை என்றால், அல்லது
 '-' (மேற்கோள்கள் இல்லாமல்) நிலையான உள்ளீடு பயன்படுத்தப்படுகிறது.     பொதுவாக, பயனர் ஐடி மற்றும் குழு ஐடி இருந்தால் இந்த ச்கிரிப்ட் இயக்க மறுக்கும்
 'மெயில்மேன்' பயனர் மற்றும் குழுவிற்கு அமைக்கப்படவில்லை (நீங்கள் கட்டமைக்கும்போது வரையறுக்கப்பட்டுள்ளது
 அஞ்சல் வீரர்). ரூட்டாக இயக்கப்பட்டால், இந்த ச்கிரிப்ட் இந்த பயனர் மற்றும் குழுவிற்கு மாறும்
 காசோலை செய்யப்படுவதற்கு முன்.

 சோதனை மற்றும் பிழைத்திருத்த நோக்கங்களுக்கு இது சிரமமாக இருக்கும், எனவே -u கொடி
 UID/GID ஐ அமைக்கும் மற்றும் சரிபார்கும் படி தவிர்க்கப்பட்டிருக்கும், மற்றும்
 நிரல் தற்போதைய பயனர் மற்றும் குழுவாக இயக்கப்படுகிறது. இந்த கொடி பரிந்துரைக்கப்படவில்லை
 சாதாரண விளைவாக்கம் சூழல்களுக்கு.

 இருப்பினும், நீங்கள் -u உடன் ஓடி, மெயில்மேன் குழுவில் இல்லாவிட்டால், நீங்கள்
 ஒரு பட்டியலை நீக்க முடியாதது போன்ற இசைவு சிக்கல்கள் இருக்கலாம்
 வலை வழியாக காப்பகங்கள். கடினமான அதிர்ச்டம்!     பட்டியல் உரிமையாளருக்கு அவர்களின் அஞ்சல் பட்டியல் என்று மின்னஞ்சல் மூலம் அறிவிக்கவும்
 உருவாக்கப்பட்டது.     ஒரு அஞ்சல் பட்டியலில் இயக்கவும்.

 விரிவான உதவிக்கு, -டிடெயில்களைப் பார்க்கவும்
         இந்த அஞ்சல் பட்டியலில் இயக்கவும். பல -பட்டியல் விருப்பங்கள் வழங்கப்படலாம். தி
 உரையாடல் ஒரு பட்டியல்-ஐடி அல்லது முழு தகுதி வாய்ந்த பட்டியல் பெயராக இருக்கலாம். இல்லாமல்
 இந்த விருப்பம், அனைத்து அஞ்சல் பட்டியல்களுக்கும் செரிமானங்களில் இயங்குகிறது.     இந்த அஞ்சல் பட்டியலில் இயக்கவும். பல -பட்டியல் விருப்பங்கள் வழங்கப்படலாம். தி
 உரையாடல் ஒரு பட்டியல்-ஐடி அல்லது முழு தகுதி வாய்ந்த பட்டியல் பெயராக இருக்கலாம். இல்லாமல்
 இந்த விருப்பம், அனைத்து அஞ்சல் பட்டியல்களுக்கான கோரிக்கைகளில் செயல்படுகிறது.     மாச்டர் அழைக்கும் ஓட்டப்பந்தய வீரர்களின் இயல்புநிலை தொகுப்பை மேலெழுதவும், அதாவது
 பொதுவாக உள்ளமைவு கோப்பில் வரையறுக்கப்படுகிறது. பல -ஆர் விருப்பங்கள் இருக்கலாம்
 கொடுக்கப்பட்ட. -R க்கான மதிப்புகள் நேராக பின்/ரன்னருக்கு அனுப்பப்படுகின்றன.     Admin_notify_mchanges க்கான பட்டியலின் அமைப்பை மேலெழுதவும்.     Send_goodbye_message க்கான பட்டியலின் அமைப்பை மீறவும்
 நீக்கப்பட்ட உறுப்பினர்கள்.     கூடுதல் உறுப்பினர்களுக்கு send_welcome_message க்கான பட்டியலின் அமைப்பை மீறுங்கள்.     Send_welcome_message க்கான பட்டியலின் அமைப்பை மீறவும்.     இன்னும் பதிவு செய்யப்படாவிட்டால் அஞ்சல் பட்டியலின் டொமைனை பதிவு செய்யுங்கள். இது
 இயல்புநிலை நடத்தை, ஆனால் இந்த விருப்பங்கள் பின்தங்கிய நிலையில் வழங்கப்படுகின்றன
 பொருந்தக்கூடிய தன்மை. -D உடன் அஞ்சல் பட்டியலின் களத்தை பதிவு செய்ய வேண்டாம்.     பெயரிடப்பட்ட ரன்னரை அதன் முதன்மையான வளையத்தின் மூலம் ஒரு முறை இயக்கவும். இல்லையெனில், தி
 செயல்முறை ஒரு சமிக்ஞையைப் பெறும் வரை ரன்னர் காலவரையின்றி ஓடுகிறார். இது இல்லை
 ஒரு முறை இயக்க முடியாத ஓட்டப்பந்தய வீரர்களுடன் இணக்கமானது.     ரூட் பரிந்துரைக்கப்படவில்லை மற்றும் மெயில்மேன் செய்வார் என மெயில்மேன் கட்டளைகளை இயக்குவது
 இந்த விருப்பம் குறிப்பிடப்படாவிட்டால் ரூட்டாக இயக்க மறுக்கவும்.     தேடலுக்கு பயன்படுத்த வேண்டிய பிரிவு. திறவுகோல் எதுவும் கொடுக்கப்படவில்லை என்றால், அனைத்து முக்கிய மதிப்பு சோடிகளும்
 கொடுக்கப்பட்ட பிரிவின் காண்பிக்கப்படும்.     அவற்றின் டைசச்ட்_செண்ட்_பெரியோடிக் என்றால் மட்டுமே பட்டியலுக்கு சேகரிக்கப்பட்ட செரிமானங்களை அனுப்பவும்
 உண்மை என அமைக்கப்பட்டுள்ளது.     சேகரிக்கப்பட்ட எந்த செரிமானங்களையும் இப்போதே அனுப்புங்கள், அளவு வாசல் இல்லாவிட்டாலும் கூட
 இன்னும் சந்திக்கப்பட்டன.     சேர்க்கப்பட்ட உறுப்பினர்களுக்கு உடனடியாகச் சேர்ப்பதை விட அழைப்பை அனுப்புங்கள்.
         சேர்க்கப்பட்ட உறுப்பினர்கள் விநியோக பயன்முறையை 'வழக்கமான', 'மைம்', 'வெற்று', என அமைக்கவும்
 'சுருக்கம்' அல்லது 'முடக்கப்பட்டது'. அதாவது, வழக்கமான, மூன்று முறைகளில் ஒன்று
 அல்லது டெலிவரி இல்லை. வழங்கப்படாவிட்டால், இயல்புநிலை வழக்கமானதாகும்.     சேர்க்கப்பட்ட உறுப்பினர்கள் விநியோக பயன்முறையை 'வழக்கமான', 'மைம்', 'வெற்று', என அமைக்கவும்
 'சுருக்கம்' அல்லது 'முடக்கப்பட்டது'. அதாவது, வழக்கமான, மூன்று முறைகளில் ஒன்று
 அல்லது டெலிவரி இல்லை. வழங்கப்படாவிட்டால், இயல்புநிலை வழக்கமானதாகும். அழைக்கப்பட்டதற்காக புறக்கணிக்கப்பட்டது
 உறுப்பினர்கள்.     பட்டியலின் விருப்பமான மொழியை குறியீடாக அமைக்கவும், இது பதிவுசெய்யப்பட்ட இரண்டாக இருக்க வேண்டும்
 கடிதம் மொழி குறியீடு.     பட்டியல் உரிமையாளர் மின்னஞ்சல் முகவரியைக் குறிப்பிடவும். முகவரி தற்போது இல்லை என்றால்
 மெயில்மேனுடன் பதிவுசெய்யப்பட்ட முகவரி பதிவு செய்யப்பட்டு பயனருடன் இணைக்கப்பட்டுள்ளது.
 மெயில்மேன் முகவரிக்கு உறுதிப்படுத்தும் செய்தியை அனுப்புவார், ஆனால் அதுவும் இருக்கும்
 முகவரிக்கு பட்டியல் உருவாக்கும் அறிவிப்பை அனுப்பவும். ஒன்றுக்கு மேற்பட்ட உரிமையாளர்கள் இருக்க முடியும்
 குறிப்பிடப்பட்டது.     பட்டியல் பாணி பெயரைக் குறிப்பிடவும்.     யுடிஎஃப் -8 அல்லது துணைக்குழு இல்லையென்றால் ஊறுகாய்_பிலில் சரங்களின் குறியாக்கத்தைக் குறிப்பிடவும்
 அதன். இது பொதுவாக பட்டியலின் மெயில்மேன் 2.1 சார்சமாக இருக்கும்
 விருப்பமான_ மொழி.     ஒரு ரன்னரைத் தொடங்குங்கள்.

 கட்டளை வரியில் பெயரிடப்பட்ட ரன்னர் தொடங்கப்பட்டது, அது இயங்கலாம்
 அதன் முதன்மையான வளையத்தின் மூலம் ஒரு முறை (இதை ஆதரிக்கும் அந்த ஓட்டப்பந்தய வீரர்களுக்கு) அல்லது
 தொடர்ச்சியாக. பிந்தையது மாச்டர் ரன்னர் அதன் அனைத்தையும் எவ்வாறு தொடங்குகிறது என்பதுதான்
 துணை செயலாக்கங்கள்.

 -l அல்லது -h கொடுக்கப்படாவிட்டால் -r தேவைப்படுகிறது, மேலும் அதன் உரையாடல் ஒன்றாக இருக்க வேண்டும்
 -l சுவிட்சால் காட்டப்படும் பெயர்கள்.

 பொதுவாக, இந்த ச்கிரிப்ட் `மெயில்மேன் ச்டார்ட்` இலிருந்து தொடங்கப்பட வேண்டும். அதை இயக்குகிறது
 தனித்தனியாக அல்லது -o பொதுவாக பிழைத்திருத்தத்திற்கு மட்டுமே பயனுள்ளதாக இருக்கும். ஓடும்போது
 இந்த வழியில், சுற்றுச்சூழல் மாறி $ mailman_under_master_control இருக்கும்
 சில பிழை கையாளுதல் நடத்தையை நுட்பமாக மாற்றும்.
         ஒரு ஊடாடும் பைதான் அமர்வைத் தொடங்கவும், 'எம்' எனப்படும் மாறியுடன்
 தேர்ந்தெடுக்கப்படாத பொருட்களின் பட்டியலைக் கொண்டுள்ளது.     பொருள்: $ {subject}     கேடனெவ்ச் கட்டளை அவ்வப்போது என்என்டிபி ரன்னரால் இயக்கப்படுகிறது.
 நீங்கள் அதை க்ரோன் வழியாக இயக்குகிறீர்கள் என்றால், அதை க்ரோன்டாப்பிலிருந்து அகற்ற வேண்டும்.
 நீங்கள் அதை கைமுறையாக இயக்க விரும்பினால், _mailman_gatenews_nntp ஐ அமைக்கவும்
 சூழல்.     செயல்பட வேண்டிய பட்டியல். -ஃப்ரோமால் குறிப்பிடப்படாவிட்டால் தேவை.
         செய்தியை செலுத்த வரிசையின் பெயர். வரிசை ஒன்றாக இருக்க வேண்டும்
 வரிசை கோப்பகத்திற்குள் உள்ள கோப்பகங்கள். விடுபட்டால், உள்வரும் வரிசை
 பயன்படுத்தப்பட்டது.     சேர்க்க/நீக்க பங்கு. இது 'உரிமையாளர்' அல்லது 'மதிப்பீட்டாளர்' ஆக இருக்கலாம்.
 கொடுக்கப்படாவிட்டால், 'உரிமையாளர்' பங்கு கருதப்படுகிறது.
         இந்த விருப்பம் எந்த விளைவையும் ஏற்படுத்தாது. இது பின்னோக்கி பொருந்தக்கூடிய தன்மைக்கு மட்டுமே உள்ளது.     கொடுக்கப்பட்ட பாத்திரத்துடன் சேர்க்க பயனர். இது ஒரு மின்னஞ்சல் முகவரியாக இருக்கலாம் அல்லது என்றால்
 மேற்கோள் காட்டப்பட்டால், எந்த காட்சி பெயர் மற்றும் மின்னஞ்சல் முகவரி பாகுபடுத்தக்கூடியது
 மின்னஞ்சல்.utils.parseaddr. எ.கா., 'சேன் டோ <jane@example.com>'. மீண்டும் மீண்டும் இருக்கலாம்
 பல பயனர்களைச் சேர்க்க.
         [பயன்முறை] அனைத்து உறுப்பினர் முகவரிகளையும் கோப்பு பெயரில் சேர்க்கவும். இந்த விருப்பம் அகற்றப்பட்டது.
 அதற்கு பதிலாக 'மெயில்மேன் சேர்க்கை' பயன்படுத்தவும்.     [பயன்முறை] கோப்பு பெயரில் காணப்படும் அனைத்து உறுப்பினர் முகவரிகளையும் நீக்கு.
 இந்த விருப்பம் அகற்றப்பட்டது. அதற்கு பதிலாக 'மெயில்மேன் டெல்மெம்பர்ச்' ஐப் பயன்படுத்தவும்.     [பயன்முறை] குறிப்பிட்ட அஞ்சல் பட்டியலின் அனைத்து உறுப்பினர் முகவரிகளையும் ஒத்திசைக்கவும்
 கோப்பு பெயரில் காணப்படும் உறுப்பினர் முகவரிகளுடன்.
 இந்த விருப்பம் அகற்றப்பட்டது. அதற்கு பதிலாக 'மெயில்மேன் ஒத்திசைவர்கள்' பயன்படுத்தவும்.  (டைசச்ட் பயன்முறை) $ {count} பொருந்தும் அஞ்சல் பட்டியல்கள் காணப்படுகின்றன: $ {display_name} போச்ட் ஒப்புதல் $ {மின்னஞ்சல் ஏற்கனவே $ {mlist.fqdn_listname of இன் $ {email} $ {மின்னஞ்சல் அச் $ {mlist.fqdn_listname of இன் $ {email} அல்ல $ {member} பவுன்ச் காரணமாக $ {mlist.display_name} அஞ்சல் பட்டியலிலிருந்து குழுவிலகப்படாதது $ {member {member} இல் அதிகரிக்கப்பட்டது. $ {member} இல் முடக்கப்பட்டுள்ளது $ {உறுப்பினர் சந்தா முடக்கப்பட்டுள்ளது $ {mlist.display_name} டைசச்ட், தொகுதி $ {volume}, வெளியீடு $ {digest_number} $ {mlist.display_name} அஞ்சல் பட்டியல் ஆய்வு செய்தி $ {mlist.display_name} சந்தா அறிவிப்பு $ {mlist.display_name} குழுவிலக அறிவிப்பு $ {msg.sender இலிருந்து இடுகையிட்ட $ {mlist.fqdn_listname} ஒப்புதல் தேவை $ {mlist.list_id} தொகுதிக்கு ஏற்றப்பட்டது $ {mlist.volume}, எண் $ {mlist.next_digest_number} $ {mlist.list_id} உறுப்பினர்கள் இல்லை $ {mlist.list_id} தொகுதிகளில் உள்ளது $ {mlist.volume}, எண் $ {mlist.next_digest_number} $ {mlist.list_id} அனுப்பப்பட்ட தொகுதி $ {mlist.volume}, எண் $ {mlist.next_digest_number} $ {name} இயங்குகிறது $ {classname} $ {நபர் the $ {person} $ {person} இடது $ {mlist.fqdn_listname} $ {realname} வழியாக $ {mlist.display_name} $ {self.email} ஏற்கனவே அஞ்சல் பட்டியலில் உறுப்பினராக உள்ளார் $ {self.fqdn_listname} $ {self.email} என்பது ஏற்கனவே அஞ்சல் பட்டியலின் மதிப்பீட்டாளராக உள்ளது {self.fqdn_listname} $ {self.email} என்பது ஏற்கனவே அஞ்சல் பட்டியலில் உறுப்பினர் அல்லாதவர் $ {self.fqdn_listname} $ {self.email} என்பது ஏற்கனவே அஞ்சல் பட்டியலின் உரிமையாளர் $ {self.fqdn_listname} $ {self.name}: $ {மின்னஞ்சல் the $ {email} இன் உறுப்பினர் அல்ல $ {self.name}: குழுசேர சரியான முகவரி எதுவும் கிடைக்கவில்லை $ {self.name}: குழுவிலகுவதற்கு சரியான மின்னஞ்சல் முகவரி இல்லை $ {self.name}: அத்தகைய கட்டளை இல்லை: $ {command_name} $ {self.name}: பல வாதங்கள்: $ {printable_arguments} (பொருள் இல்லை) - அசல் செய்தி விவரங்கள்: -------------- அடுத்த பகுதி --------------
 -அனுப்பப்பட்ட மற்றும்-பீரியோடிக் கொடிகளை ஒன்றாகப் பயன்படுத்த முடியாது <----- பொருள் {{count} -----> ஐத் தொடங்குங்கள் எளிய உரை செரிமானங்களுடன் பொருந்தாத ஒரு செய்தி பகுதி அகற்றப்பட்டுள்ளது ...
 பெயர்: $ {filename}
 வகை: $ {ctype}
 அளவு: $ {size} பைட்டுகள்
 Desc: $ {desc}
 குனு மெயில்மேனின் முந்தைய ஓட்டம் சுத்தமாக வெளியேறவில்லை ({}). -ஃபோர்ச் பயன்படுத்த முயற்சிக்கவும் எப்போதும் பொருந்தக்கூடிய ஒரு விதி. ஒரு செய்தியை ஏற்றுக்கொள்ளுங்கள். பொருள் தலைப்பு மதிப்பில் பட்டியல்-குறிப்பிட்ட முன்னொட்டைச் சேர்க்கவும். RFC 2369 பட்டியல்-* தலைப்புகளைச் சேர்க்கவும். காப்பகங்களில் செய்தியைச் சேர்க்கவும். டைசெச்டில் செய்தியைச் சேர்க்கவும், அதை அனுப்பலாம். முகவரி {} இலிருந்து {to ஆக மாற்றப்பட்டது. முகவரி}} ஏற்கனவே உள்ளது; மாற்ற முடியாது. முகவரி {the சரியான மின்னஞ்சல் முகவரி அல்ல. முகவரி}} காணப்படவில்லை. முகவரிகள் வேறுபட்டவை அல்ல. மாற்ற எதுவும் இல்லை. உள்ளடக்க வடிகட்டலுக்குப் பிறகு, செய்தி காலியாக இருந்தது ஏற்கனவே சந்தா (தவிர்க்கவும்): $ {email} 'முடிவு' என்பதற்கான மாற்றுப்பெயர். 'சேர' ஒரு மாற்றுப்பெயர். 'விடுப்பு' என்பதற்கான மாற்றுப்பெயர். பல்வேறு எம்.டி.ஏ கோப்புகளை வெளியிடுவதற்கான மாற்று அடைவு. இயங்குவதன் மூலம் பட்டியலின் கோரிக்கை முகவரியை அச்சிடலாம்:

 % மெயில்மேன் withlist -r listaddr.requestaddr -l ant@example.com
 ListAddr ஐ இறக்குமதி செய்கிறது ...
 Listaddr.requestaddr () ஐ இயக்குகிறது ...
 ant-request@example.com அஞ்சல் பட்டியல் பாணியை மட்டும் அறிவிக்கவும். DMARC தணிப்புகளைப் பயன்படுத்துங்கள். மற்றொரு எடுத்துக்காட்டு, ஒரு குறிப்பிட்ட காட்சி பெயரை மாற்ற விரும்புகிறீர்கள் என்று சொல்லுங்கள்
 அஞ்சல் பட்டியல். பின்வரும் செயல்பாட்டை நீங்கள் அழைக்கப்படும் கோப்பில் வைக்கலாம்
 `Chance.py`:

 DEF CHANGE (MLIST, Display_name):
 mlist.display_name = display_name

 இதை கட்டளை வரியிலிருந்து இயக்கவும்:

 % mailman withlist -r மாற்றம் -l ant@example.com 'எனது பட்டியல்'

 எந்தவொரு தரவுத்தள பரிவர்த்தனைகளையும் நீங்கள் வெளிப்படையாக செய்ய வேண்டியதில்லை என்பதை நினைவில் கொள்க
 மெயில்மேன் உங்களுக்காக இதைச் செய்வார் (பிழைகள் எதுவும் ஏற்படவில்லை என்று கருதி). உங்கள் செய்தியை "$ {display_name}" அஞ்சல் பட்டியலுக்கு தானாக பதிலளிக்கவும் மோசமான ரன்னர் ச்பெக்: $ {value} உரிமையாளர் மற்றும் மதிப்பீட்டாளர் பெறுநர்களைக் கணக்கிடுங்கள். செய்தியின் வழக்கமான பெறுநர்களைக் கணக்கிடுங்கள். ரன்னர் தொகுதி இறக்குமதி செய்ய முடியாது: $ {class_path} செல்லுபடியாகும் மின்னஞ்சல் முகவரியாக (ச்கிப்பிங்) அலச முடியாது: $ {line} செய்தியை அவிழ்க்க முடியாது $ {filebase}, தவிர்க்க:
 $ {error} குறிப்பிட்ட அதிகபட்சத்தை விட பெரிய செய்திகளைப் பிடிக்கவும். டைசச்ட் பொருள் அல்லது கொதிகலன் மேற்கோளுடன் செய்திகளைப் பிடிக்கவும். மறைமுகமான இலக்குடன் செய்திகளைப் பிடிக்கவும். இல்லை, அல்லது வெற்று, பொருள் தலைப்புகள் கொண்ட செய்திகளைப் பிடிக்கவும். சந்தேகத்திற்கிடமான தலைப்புகளுடன் செய்திகளைப் பிடிக்கவும். பல வெளிப்படையான பெறுநர்களுடன் செய்திகளைப் பிடிக்கவும். தவறாக முகவரிக்கு அனுப்பப்பட்ட மின்னஞ்சல் கட்டளைகளைப் பிடிக்கவும். எல்லா செய்திகளிலிருந்தும் சில தலைப்புகளை சுத்தப்படுத்தவும். சந்தா அல்லது வைத்திருக்கும் செய்தி கோரிக்கையை உறுதிப்படுத்தவும். உறுதிப்படுத்தல் மின்னஞ்சல் $ {person} க்கு அனுப்பப்பட்டது உறுதிப்படுத்தல் மின்னஞ்சல் $ {நபருக்கு அனுப்பப்பட்டது $ {person} உறுதிப்படுத்தல் கிள்ளாக்கு பொருந்தவில்லை உறுதிப்படுத்தப்பட்டது உள்ளடக்க வடிகட்டி செய்தி அறிவிப்பு உருவாக்கிய அஞ்சல் பட்டியல்: $ {mlist.fqdn_listname} DMARC மிதமான தலைப்புகள் மற்றும் அடிக்குறிப்புகளுடன் ஒரு செய்தியை அலங்கரிக்கவும். அடிக்குறிப்பை டைசச்ட் செய்யுங்கள் தலைப்பு தலைப்பு ஒரு செய்தியை நிராகரித்து செயலாக்கத்தை நிறுத்துங்கள். தனியார் காப்பகங்களுடன் கலந்துரையாடல் அஞ்சல் பட்டியல் பாணி. மெயில்மேனின் பதிப்பைக் காண்பி. பதிவு கோப்பில் மேலும் பிழைத்திருத்த தகவல்களைக் காண்பி. உங்கள் வாதங்களை மீண்டும் எதிரொலிக்கவும். மின்னஞ்சல்: {} இந்த பட்டியலுக்கு அவசரகால மிதமானது நடைமுறையில் உள்ளது முடிவு  செய்திகளின் MIME உள்ளடக்கத்தை வடிகட்டவும். இதன் மூலம் DMARC கொள்கையைக் கண்டறியவும்: டொமைன். அறியப்படாத காரணங்களுக்காக, மாச்டர் லாக் பெற முடியவில்லை.

 பூட்டு கோப்பு: $ {config.LOCK_FILE}
 பூட்டு ஓச்ட்: $ {hostname}

 வெளியேறுதல். மிதமான செய்தியை முன்னோக்கி குனு மெயில்மேன் ஏற்கனவே இயங்குகிறார் குனு மெயில்மேன் எதிர்பாராத நிலையில் உள்ளார் ($ {hostname}! = $ {fqdn_name}) குனு மெயில்மேன் இயங்கவில்லை குனு மெயில்மேன் இயங்குகிறார் (மாச்டர் பிஐடி: $ {pid}) குனு மெயில்மேன் நிறுத்தப்பட்டார் (பழைய பிஐடி: $ {pid}) எம்.டி.ஏ மாற்றுப்பெயர் வரைபடங்களை உருவாக்குதல் பட்டியல் உறுப்பினர்களின் பட்டியலைப் பெறுங்கள். கிடைக்கக்கூடிய மின்னஞ்சல் கட்டளைகளைப் பற்றிய உதவியைப் பெறுங்கள். வரிசை கோப்பிலிருந்து தகவல்களைப் பெறுங்கள். சேர்க்கும் கோப்பிலிருந்து சாதாரண விநியோக பெறுநர்களைப் பெறுங்கள். தலைப்பு "{}" ஒரு பவுன்ச்_மாட்சிங்_எடர் வரியுடன் பொருந்தியது தலைப்பு "{}" ஒரு தலைப்பு விதியுடன் பொருந்தியது -ரன் விருப்பத்தை எவ்வாறு பயன்படுத்துவது என்பதற்கான எடுத்துக்காட்டு இங்கே. உங்களிடம் ஒரு கோப்பு உள்ளது என்று சொல்லுங்கள்
 மெயில்மேன் நிறுவல் அடைவு 'ListAddr.py' என்று அழைக்கப்படுகிறது, பின்வரும் இரண்டோடு
 செயல்பாடுகள்:

 def listaddr (Mlist):
 அச்சு (mlist.posting_address)

 def requestaddr (mlist):
 அச்சு (mlist.request_address)

 ரன் முறைகள் குறைந்தது ஒரு வாதத்தை எடுத்துக் கொள்ளுங்கள், அஞ்சல் பட்டியல் செயல்பட பொருள்
 ஆன். கட்டளை வரியில் கொடுக்கப்பட்ட கூடுதல் வாதங்கள் எதுவும் அனுப்பப்படுகின்றன
 அழைக்கக்கூடிய நிலை வாதங்கள்.

 -எல் வழங்கப்படாவிட்டால், எந்த வாதங்களையும் எடுக்காத ஒரு செயல்பாட்டை நீங்கள் இயக்க முடியும்.
 ஒரு செய்தியை வைத்து செயலாக்கத்தை நிறுத்துங்கள். இந்த செய்திக்கு நீங்கள் பதிலளித்தால், விசயத்தை வைத்திருத்தல்: தலைப்பு அப்படியே, மெயில்மேன் செய்வார்
 வைத்திருக்கும் செய்தியை நிராகரிக்கவும். செய்தி ச்பேம் என்றால் இதைச் செய்யுங்கள். நீங்கள் பதிலளித்தால்
 இந்த செய்தி மற்றும் அங்கீகரிக்கப்பட்டவை: அதில் உள்ள பட்டியல் கடவுச்சொல்லுடன் தலைப்பு, தி
 பட்டியலில் இடுகையிட செய்தி அங்கீகரிக்கப்படும். அங்கீகரிக்கப்பட்ட: தலைப்பு முடியும்
 பதிலின் உடலின் முதல் வரியிலும் தோன்றும். அகராதியை புறக்கணித்தல்: {0! R} உரை அல்லாத/வெற்று மைம் பகுதிகளை புறக்கணித்தல் சட்டவிரோத பட்டியல் பெயர்: $ {fqdn_listname} சட்டவிரோத உரிமையாளர் முகவரிகள்: $ {invalid} இந்த மெயில்மேன் நிகழ்வு பற்றிய தகவல்கள். ஒரு கோப்பிலிருந்து ஒரு செய்தியை ஒரு அஞ்சல் பட்டியலின் வரிசையில் செலுத்துங்கள். தவறான அங்கீகாரம்: கடவுச்சொல் தவறான மின்னஞ்சல் முகவரி: $ {email} தவறான மொழி குறியீடு: $ {language_code} தவறான அல்லது சரிபார்க்கப்படாத மின்னஞ்சல் முகவரி: $ {email} [செல்] USE_PYTHON க்கான தவறான மதிப்பு: {} மாச்டர் கூட ஓடுகிறாரா? இந்த அஞ்சல் பட்டியலில் சேரவும். இன்றைய கடைசி ஆட்டார்ச்போன்ச் அறிவிப்பு இந்த அஞ்சல் பட்டியலை விட்டு விடுங்கள். இந்த அஞ்சல் பட்டியலை விட்டு விடுங்கள்.

 உங்கள் கோரிக்கையை உறுதிப்படுத்த உங்களிடம் கேட்கப்படலாம். குறைவான சொற்களஞ்சியம் அனைத்து அஞ்சல் பட்டியல்களையும் பட்டியலிடுங்கள். பட்டியல் ஏற்கனவே உள்ளது: $ {fqdn_listname} பகிரங்கமாக விளம்பரப்படுத்தப்பட்ட அந்த அஞ்சல் பட்டியல்களை மட்டுமே பட்டியலிடுங்கள் கிடைக்கக்கூடிய ரன்னர் பெயர்களை பட்டியலிட்டு வெளியேறவும். பட்டியல்: {} இடுகையிடும் வளையத்தைத் தேடுங்கள். முந்தைய விதி வெற்றியைப் பாருங்கள். ஒரு அஞ்சல் பட்டியலில் இடுகையிடப்பட்ட அனைத்து செய்திகளையும் பொருத்தவும்
 மிதமான செய்திக்குழு.
         தடைசெய்யப்பட்ட முகவரிகளால் அனுப்பப்பட்ட செய்திகளை பொருத்தவும். மிதமான உறுப்பினர்கள் அனுப்பிய செய்திகளை பொருத்துங்கள். பொருந்தாதவர்கள் அனுப்பிய செய்திகளை பொருத்துங்கள். சரியான அனுப்புநர்கள் இல்லாமல் செய்திகளை பொருத்துங்கள். உறுப்பினர் சந்தா செலுத்தவில்லை (தவிர்க்கவும்): $ {email} }} அஞ்சல் பட்டியலின் உறுப்பினர்கள்:
 {} உறுப்பினர் தடைசெய்யப்பட்டுள்ளது (தவிர்க்கவும்): $ {email} செய்தி $ {message} செய்தியில் நிர்வாகி உள்ளது செய்தியில் டைசச்ட் பொருள் உள்ளது செய்தி ஏற்கனவே இந்த பட்டியலில் வெளியிடப்பட்டுள்ளது செய்தியில் மறைமுகமான இலக்கு உள்ளது செய்தியில்}} பெறுநர்களை விட அதிகமாக உள்ளது செய்திக்கு எந்த விசயமும் இல்லை செய்தி மேற்கோள்கள் கொதிகலன் டைசச்ட் செய்தி அனுப்புநர் இந்த பட்டியலில் இருந்து தடைசெய்யப்பட்டுள்ளது கோரிக்கைக்கான தரவைக் காணவில்லை {}

 மிதமான சங்கிலி செய்தி தலைப்புகளை மாற்றவும். வெளிச்செல்லும் செய்தி வரிசையில் செய்தியை நகர்த்தவும். இதற்கில்லை பெயர்: $ {fname}
 $ {Self.mlist.display_name க்கு க்கு புதிய சந்தா கோரிக்கை $ {self.mlist.display_name} $ {Mlist.display_name இருந்து இலிருந்து புதிய குழுவிலகும் கோரிக்கை $ {mlist.display_name} $ {Self.mlist.display_name க்கு க்கு புதிய குழுவிலகும் கோரிக்கை $ {self.mlist.display_name} PID உடன் குழந்தை இல்லை: $ {pid} உறுதிப்படுத்தல் கிள்ளாக்கு எதுவும் கிடைக்கவில்லை பொருந்தக்கூடிய அஞ்சல் பட்டியல்கள் எதுவும் கிடைக்கவில்லை மின்னஞ்சல் முகவரிக்கு பதிவுசெய்யப்பட்ட பயனர் இல்லை: $ {email} ரன்னர் பெயர் கொடுக்கப்படவில்லை. செய்தியில் அனுப்புநர் எதுவும் காணப்படவில்லை. அத்தகைய கட்டளை இல்லை: $ {command_name} அத்தகைய பட்டியல் எதுவும் கிடைக்கவில்லை: $ {spec} அத்தகைய பட்டியல் பொருந்தாது விவரக்குறிப்பு: $ {listspec} அத்தகைய பட்டியல் இல்லை: $ {_list} அத்தகைய பட்டியல் இல்லை: $ {listspec} அத்தகைய வரிசை இல்லை: $ {queue} ஒரு மெயில்மேன் 2.1 உள்ளமைவு கோப்பு: $ {pickle_file} சேர்க்க அல்லது நீக்க எதுவும் இல்லை. செய்ய எதுவும் இல்லை பட்டியல் உரிமையாளர்கள்/நிலுவையில் உள்ள கோரிக்கைகளின் மதிப்பீட்டாளர்களுக்கு அறிவிக்கவும். காணப்படும் பொருட்களின் எண்ணிக்கை ('எம்' மாறி பார்க்கவும்): $ {count} செரிமானங்களில் இயங்குகிறது. சாதாரண கலந்துரையாடல் அஞ்சல் பட்டியல் பாணி. அசல் செய்தி பிஐடி படிக்க முடியாதது: $ {config.PID_FILE} வில் அங்கீகார சோதனைகளைச் செய்து, இதன் விளைவாக வரும் தலைப்புகளை இணைக்கவும் அங்கீகார சோதனைகளைச் செய்து அங்கீகார-தீர்மானங்கள் தலைப்பை இணைக்கவும். வெற்றிகரமான இடுகைக்குப் பிறகு சில புத்தக பராமரிப்புகளைச் செய்யுங்கள். அஞ்சல் பட்டியல்களுக்கு நுழைவாயிலாக செய்திகளுக்கு என்என்டிபி சேவையகத்தை வாக்கெடுப்பு. மிதமான செய்திக்குழு நுழைவாயிலில் இடுங்கள் "$ {subject}" என்ற தலைப்பில் உங்கள் செய்தியை இடுகையிடுவது விரிவான வழிமுறைகளை அச்சிட்டு வெளியேறவும். குறைந்த வெளியீட்டை அச்சிடுங்கள். சில கூடுதல் நிலையை அச்சிடுக. மெயில்மேன் உள்ளமைவை அச்சிடுக. செயலாக்க DMARC தணிப்புகளை நிராகரிக்கிறது அல்லது நிராகரிக்கவும் உறுப்பினர் பெயர்கள் மற்றும் மின்னஞ்சல் முகவரிகளின் பட்டியலை உருவாக்குகிறது.

 அறிக்கையை மட்டுப்படுத்த விருப்ப விநியோக = மற்றும் பயன்முறை = வாதங்கள் பயன்படுத்தப்படலாம்
 பொருந்தக்கூடிய விநியோக நிலை மற்றும்/அல்லது விநியோக முறை கொண்ட உறுப்பினர்களுக்கு. என்றால்
 டெலிவரி = அல்லது பயன்முறை = ஒன்றுக்கு மேற்பட்ட முறை குறிப்பிடப்பட்டுள்ளது, கடைசி நிகழ்வு மட்டுமே
 பயன்படுத்தப்படுகிறது.
 நிரலாக்க ரீதியாக, நீங்கள் ஒரு அஞ்சல் பட்டியலில் செயல்பட ஒரு செயல்பாட்டை எழுதலாம், மற்றும்
 இந்த ச்கிரிப்ட் வீட்டு பராமரிப்பை கவனித்துக்கொள்ளும் (எடுத்துக்காட்டுகளுக்கு கீழே காண்க). இல்
 அந்த வழக்கு, பொதுவான பயன்பாட்டு தொடரியல்:

 % மெயில்மேன் வித்ச்லிச்ட் [விருப்பங்கள்] -எல் லிச்ட்ச்பெக் [ஆர்க்ச் ...]

 `பட்டியல் ச்பெக்` என்பது அஞ்சல் பட்டியலின் இடுகையிடும் முகவரி
 . காரணம்: {}

 உங்கள் MTA க்கு பொருத்தமான மாற்றுப்பெயர்களை மீண்டும் உருவாக்கவும். வழக்கமான வெளிப்பாட்டிற்கு -ரன் தேவை ஒரு செய்தியை நிராகரிக்கவும்/துள்ளவும், செயலாக்கத்தை நிறுத்தவும். டொமைன் கீச் தலைப்புகளை அகற்று. ஒரு அஞ்சல் பட்டியலை அகற்று. அகற்றப்பட்ட பட்டியல்: $ {listspec} மெயில்மேன் ஓட்டப்பந்தய வீரர்களை மீண்டும் திறக்கிறது "$ {display_name}" நிராகரிக்கப்பட்ட அஞ்சல் பட்டியலுக்கான கோரிக்கை மெயில்மேன் ஓட்டப்பந்தய வீரர்களை மறுதொடக்கம் செய்தல் ஒரு இடுகையின் ஒப்புதலை அனுப்பவும். தானியங்கி பதில்களை அனுப்பவும். வெளிச்செல்லும் வரிசையில் செய்தியை அனுப்பவும். அனுப்புநர்: {}
 கிடைக்கக்கூடிய அனைத்து வரிசை பெயர்களின் பட்டியலையும் வெளியேறுங்கள் மற்றும் வெளியேறவும். பட்டியல் விளக்கங்களையும் காட்டு பட்டியல் பெயர்களையும் காட்டு மெயில்மேன் அமைப்பின் தற்போதைய இயங்கும் நிலையைக் காட்டுங்கள். இந்த உதவி செய்தியைக் காட்டி வெளியேறவும். மெயில்மேனின் மாச்டர் ரன்னரை மூடுவது மெயில்மேன் அவர்களின் பதிவு கோப்புகளை மீண்டும் திறக்க செயல்முறைகளை சமிக்ஞை செய்யுங்கள். பழைய PID கோப்பு அகற்றப்பட்டது. மெயில்மேன் மாச்டர் மற்றும் ரன்னர் செயல்முறைகளைத் தொடங்கவும். தொடக்க மெயில்மேனின் மாச்டர் ரன்னர் மெயில்மேன் ரன்னர் துணை செயலாக்கங்களை நிறுத்தி மறுதொடக்கம் செய்யுங்கள். செயலாக்க கட்டளைகளை நிறுத்துங்கள். மெயில்மேன் மாச்டர் மற்றும் ரன்னர் செயல்முறைகளை நிறுத்துங்கள். பொருள்: {}
 சந்தா ஏற்கனவே நிலுவையில் உள்ளது (தவிர்க்கவும்): $ {email} சந்தா கோரிக்கை அதே செய்தியின் சில நகல்களை அடக்கவும். நிலை செய்திகளை அடக்கவும் தலைப்பு போட்டிகளுடன் செய்திகளைக் குறிக்கவும். மெயில்மேன் பதில் போட் இணைக்கப்பட்ட செய்தி $ {mlist.display_name} அஞ்சல் பட்டியலின் உள்ளடக்கத்துடன் பொருந்தியது
 வடிகட்டுதல் விதிகள் மற்றும் பட்டியலில் அனுப்பப்படுவதைத் தடுக்கவும்
 உறுப்பினர். நிராகரிக்கப்பட்டவர்களின் மீதமுள்ள ஒரே நகலை நீங்கள் பெறுகிறீர்கள்
 செய்தி.

 உள்ளமைக்கப்பட்ட -உரிமையாளர் இடுகையிடும் சங்கிலி. உள்ளமைக்கப்பட்ட தலைப்பு பொருந்தும் சங்கிலி உள்ளமைக்கப்பட்ட மிதமான சங்கிலி. உள்ளமைக்கப்பட்ட உரிமையாளர் குழாய். உள்ளமைக்கப்பட்ட இடுகையிடும் குழாய். இந்த செய்தியின் உள்ளடக்கம் இழந்தது. இது அநேகமாக குறுக்கு இடுகையிடப்பட்டது
 பல பட்டியல்கள் மற்றும் முன்னர் மற்றொரு பட்டியலில் கையாளப்பட்டது.
 அஞ்சல் பட்டியல் அவசரகாலத்தில் உள்ளது, இந்த செய்தி இல்லை
 பட்டியல் நிர்வாகியால் முன்கூட்டியே அங்கீகரிக்கப்பட்டது.
         மாச்டர் பூட்டை வாங்க முடியவில்லை, ஏனெனில் அது இன்னொன்று போல் தோன்றுகிறது
 மாச்டர் ஏற்கனவே இயங்குகிறார். மாச்டர் லாக் பெற முடியவில்லை, ஏனென்றால் அது சில செயல்முறைகள் போல் தோன்றுகிறது
 வேறு சில ஓச்டில் அதைப் பெற்றிருக்கலாம். பழமையான பூட்டுகளை நாங்கள் சோதிக்க முடியாது
 எல்லைகளை புரவலன் செய்யுங்கள், எனவே இதை நீங்கள் கைமுறையாக தூய்மை செய்ய வேண்டும்.

 பூட்டு கோப்பு: $ {config.LOCK_FILE}
 பூட்டு ஓச்ட்: $ {hostname}

 வெளியேறுதல். மாச்டர் லாக் பெற முடியவில்லை. இது ஒரு பழமையானது போல் தோன்றுகிறது
 முதன்மை பூட்டு. --ஃபோர்ச் கொடியுடன் $ {நிரலை மீண்டும் இயக்க முயற்சிக்கவும். செய்தி ஒரு மிதமான உறுப்பினரிடமிருந்து வருகிறது செய்தியில் பொருந்தக்கூடிய ஒப்புதல் அல்லது அங்கீகரிக்கப்பட்ட தலைப்பு உள்ளது. செய்திக்கு சரியான அனுப்புநர்கள் இல்லை செய்தி {} kb அதிகபட்ச அளவை விட பெரியது செய்தி பட்டியல் உறுப்பினரிடமிருந்து அல்ல செய்தியின் உள்ளடக்க வகை வெளிப்படையாக அனுமதிக்கப்படவில்லை செய்தியின் உள்ளடக்க வகை வெளிப்படையாக அனுமதிக்கப்படவில்லை செய்தியின் கோப்பு நீட்டிப்பு வெளிப்படையாக அனுமதிக்கப்படவில்லை செய்தியின் கோப்பு நீட்டிப்பு வெளிப்படையாக அனுமதிக்கப்படவில்லை உங்கள் மின்னஞ்சல் கட்டளையின் முடிவுகள் கீழே கொடுக்கப்பட்டுள்ளன.
 உங்கள் மின்னஞ்சல் கட்டளைகளின் முடிவுகள் அனுப்புநர் nonmember {} பட்டியலில் உள்ளார் 'M' மாறி $ {listspec} அஞ்சல் பட்டியல் கன்னி வரிசை குழாய். {} பட்டியலில் {} மிதமான கோரிக்கைகள் காத்திருக்கின்றன. இந்த ச்கிரிப்டைப் பயன்படுத்த இரண்டு வழிகள் உள்ளன: ஊடாடும் வகையில் அல்லது நிரலாக்க ரீதியாக.
 அதைப் பயன்படுத்துவது ஒரு அஞ்சலுடன் விளையாடவும், ஆராயவும் மாற்றவும் உங்களை அனுமதிக்கிறது
 பைத்தானின் ஊடாடும் மொழிபெயர்ப்பாளரிடமிருந்து பட்டியல். ஊடாடும் போது, தி
 மாறி 'எம்' உலகளாவிய பெயர்வெளியில் கிடைக்கும். அது குறிக்கும்
 அஞ்சல் பட்டியல் பொருள். இந்த ச்கிரிப்ட் உங்களுக்கு தொடர்புகொள்வதற்கான பொதுவான கட்டமைப்பை வழங்குகிறது
 அஞ்சல் பட்டியல். இன்றைய தலைப்புகள் ($ {count} செய்திகள்) இன்றைய தலைப்புகள்:
 அறியப்படாத பவுன்ச் அறிவிப்பு வரையறுக்கப்படாத டொமைன்: $ {domain} வரையறுக்கப்படாத ரன்னர் பெயர்: $ {name} அறியப்படாத பட்டியல் பாணி பெயர்: $ {style_name} அங்கீகரிக்கப்படாத அல்லது தவறான உரையாடல் (கள்):
 {} அவிழ்க்கப்பட்ட செய்திகள். குழுவிலக கோரிக்கை பயனர்: {}
 "$ {mlist.display_name}" அஞ்சல் பட்டியல் $ {digmode} இதிலிருந்து இந்த அஞ்சல் பட்டியலில் இடுகையிட உங்களுக்கு இசைவு இல்லை: நிராகரிக்க அல்லது தனிமைப்படுத்தப்பட்ட ஒரு டி.எம்.ஏ.சி. உங்கள் செய்திகள் பிழையாக நிராகரிக்கப்படுகின்றன என்று நீங்கள் நினைத்தால், அஞ்சல் பட்டியல் உரிமையாளரை $ {listowner} என்ற எண்ணில் தொடர்பு கொள்ளவும். உறுப்பினர் பட்டியலைக் காண உங்களுக்கு ஏற்பு இல்லை. பின்வருவனவற்றிலிருந்து இயக்குவதன் மூலம் பட்டியலின் இடுகையிடும் முகவரியை நீங்கள் அச்சிடலாம்
 கட்டளை வரி:

 % மெயில்மேன் withlist -r listaddr -l ant@example.com
 ListAddr ஐ இறக்குமதி செய்கிறது ...
 Listaddr.listaddr ()
 ant@example.com {event.mlist.fqdn_listname} அஞ்சல் பட்டியலில் சேர நீங்கள் அழைக்கப்பட்டுள்ளீர்கள். $ {mlist.display_name} அஞ்சல் பட்டியலிலிருந்து நீங்கள் குழுவிலகப்பட்டீர்கள் உங்கள் சந்தா கோரிக்கையை உறுதிப்படுத்தும்படி கேட்கப்படுவீர்கள், உங்களுக்கு வழங்கப்படலாம்
 தற்காலிக கடவுச்சொல்.

 'டைசச்ட்' விருப்பத்தைப் பயன்படுத்துவதன் மூலம், நீங்கள் டைசச்ட் டெலிவரி வேண்டுமா என்பதைக் குறிப்பிடலாம்
 அல்லது இல்லை. குறிப்பிடப்படாவிட்டால், அஞ்சல் பட்டியலின் இயல்புநிலை விநியோக முறை இருக்கும்
 பயன்படுத்தப்பட்டது. முகவரியின் சந்தாவைக் கோர 'முகவரி' விருப்பத்தைப் பயன்படுத்தலாம்
 கட்டளை அனுப்புநரைத் தவிர.
 {event.mlist.fqdn_listname} அஞ்சல் பட்டியலில் சேர உங்கள் உறுதிப்படுத்தல் தேவை. {event.mlist.fqdn_listname} அஞ்சல் பட்டியலை விட்டுச் செல்ல உங்கள் உறுதிப்படுத்தல் தேவை. $ {Mlist.fqdn_listname க்கு உங்கள் செய்தி மதிப்பீட்டாளர் ஒப்புதலுக்காக காத்திருக்கிறது உங்கள் புதிய அஞ்சல் பட்டியல்: $ {fqdn_listname} $ {mlist.display_name} அஞ்சல் பட்டியலுக்கான உங்கள் சந்தா முடக்கப்பட்டுள்ளது $ {mlist.display_name} அஞ்சல் பட்டியலுக்கு உங்கள் அவசர செய்தி இல்லை
 பிரசவத்திற்கு ஏற்பு. மெயில்மேன் பெற்ற அசல் செய்தி
 இணைக்கப்பட்டுள்ளது.
 [$ {mlist.display_name}]  [----- முடிவு ஊறுகாய் -----] [----- ஊறுகாயைத் தொடங்கு -----] [சேர்] %s [Of] %s [பவுன்ச் விவரங்கள் எதுவும் கிடைக்கவில்லை] [விவரங்கள் எதுவும் கிடைக்கவில்லை] [எந்த காரணமும் கொடுக்கப்படவில்லை] [எந்த காரணங்களும் கொடுக்கப்படவில்லை] மோசமான வாதம்: $ {argument} அஞ்சல் பட்டியல் '$ பட்டியல் பெயர்' உங்களுக்காக உருவாக்கப்பட்டுள்ளது. தி
 உங்கள் அஞ்சல் பட்டியல் பற்றிய சில அடிப்படை தகவல்கள் பின்வருமாறு.

 உங்கள் பட்டியலின் பயனர்களுக்கு (நிர்வாகிகள் அல்ல) மின்னஞ்சல் அடிப்படையிலான இடைமுகம் உள்ளது;
 'உதவி' என்ற வார்த்தையுடன் ஒரு செய்தியை அனுப்புவதன் மூலம் அதைப் பயன்படுத்துவது பற்றிய தகவலைப் பெறலாம்
 பொருள் அல்லது உடலில், க்கு:

 $ request_email

 எல்லா கேள்விகளையும் $ site_email க்கு உரையாற்றவும். பட்டியல் பட்டியல் பெயர் அஞ்சல் பட்டியலுக்கான உதவி

 இது குனு மெயில்மேன் பட்டியலின் பதிப்பு $ பதிப்பிற்கான மின்னஞ்சல் கட்டளை 'உதவி'
 $ டொமைனில் மேலாளர். நீங்கள் பெற அனுப்பக்கூடிய கட்டளைகளை பின்வரும் விவரிக்கிறது
 இந்த தளத்தில் மெயில்மேன் பட்டியல்களுக்கான உங்கள் சந்தாவைப் பற்றிய தகவல்கள் மற்றும் கட்டுப்படுத்தவும்.
 ஒரு கட்டளை பொருள் வரியில் அல்லது செய்தியின் உடலில் இருக்கலாம்.

 கட்டளைகள் $ {listname} -request@$ {domain} முகவரிக்கு அனுப்பப்பட வேண்டும்.

 விளக்கங்களைப் பற்றி - "<>" இன் சொற்கள் தேவையான உருப்படிகளைக் குறிக்கின்றன
 "[]" இல் உள்ள சொற்கள் விருப்ப உருப்படிகளைக் குறிக்கின்றன. "<>" கள் அல்லது சேர்க்க வேண்டாம்
 "[]" நீங்கள் கட்டளைகளைப் பயன்படுத்தும்போது.

 பின்வரும் கட்டளைகள் செல்லுபடியாகும்:

 $ கட்டளைகள்

 ஒரு நபரின் கவனத்திற்கான கேள்விகள் மற்றும் கவலைகள் இதற்கு அனுப்பப்பட வேண்டும்:

 $ நிர்வாகி ஐபிதான் கிடைக்கவில்லை, USE_IPYTHON ஐ இல்லை என அமைக்கவும் பட்டியல் நிர்வாகியாக, உங்கள் ஏற்பு கோரப்படுகிறது
 அஞ்சல் பட்டியல் இடுகையைத் தொடர்ந்து:

 பட்டியல்: $ பட்டியல் பெயர்
 அனுப்பியவர்: $ sender_email
 பொருள்: $ பொருள்

 செய்தி நடைபெறுகிறது:

 $ காரணங்கள்

 உங்கள் வசதிக்காக, ஒப்புதல் அல்லது மறுக்க உங்கள் டாச்போர்டைப் பார்வையிடவும்
 கோரிக்கை. அஞ்சல் பட்டியல் சந்தா கோரிக்கைக்கு உங்கள் ஏற்பு தேவை
 ஒப்புதல்:

 க்கு: $ உறுப்பினர்
 பட்டியல்: $ பட்டியல் பெயர் ஒரு அஞ்சல் பட்டியல் குழுவிலக்கிற்கு உங்கள் ஏற்பு தேவை
 ஒப்புதல் கோருங்கள்:

 க்கு: $ உறுப்பினர்
 பட்டியல்: $ பட்டியல் பெயர் $ உறுப்பினரின் சந்தா அவர்களின் பவுன்ச் மதிப்பெண் காரணமாக $ பட்டியல் பெயரில் முடக்கப்பட்டுள்ளது
 அஞ்சல் பட்டியலின் பவுன்ச்_ச்கோர்_நெர்சோல்டை மீறுகிறது.

 கிடைத்தால் தூண்டுதல் டி.எச்.என் இணைக்கப்பட்டுள்ளது. $ பட்டியல் பெயரில் உறுப்பினரின் பவுன்ச் மதிப்பெண் அதிகரிக்கப்பட்டுள்ளது.

 கிடைத்தால் தூண்டுதல் டி.எச்.என் இணைக்கப்பட்டுள்ளது. $ பட்டியல் பெயர் பட்டியலில் $ எண்ணிக்கை மிதமான கோரிக்கைகள் காத்திருக்கின்றன.

 $ தரவு
 உங்கள் ஆரம்ப வசதியில் இதில் கலந்து கொள்ளுங்கள். $ உறுப்பினர் அதிகப்படியான பவுன்ச் காரணமாக $ பட்டியல் பெயரில் இருந்து குழுவிலகப்பட்டார். $ உறுப்பினர் வெற்றிகரமாக $ display_name க்கு சந்தா செலுத்தியுள்ளார். இணைக்கப்பட்ட செய்தி ஒரு துள்ளலாக பெறப்பட்டது, ஆனால் பவுன்ச் வடிவம்
 அங்கீகரிக்கப்படவில்லை, அல்லது அதிலிருந்து எந்த உறுப்பினர் முகவரிகளும் பிரித்தெடுக்க முடியாது. இது
 அங்கீகரிக்கப்படாத அனைத்து பவுன்ச் செய்திகளையும் அனுப்ப அஞ்சல் பட்டியல் கட்டமைக்கப்பட்டுள்ளது
 பட்டியல் நிர்வாகி (கள்). $ உறுப்பினர் $ display_name இலிருந்து அகற்றப்பட்டார். $ டிச்ப்ளே_பேம் அஞ்சல் பட்டியல் சமர்ப்பிப்புகளை அனுப்பவும்
 $ பட்டியல் பெயர்

 மின்னஞ்சல் வழியாக குழுசேர அல்லது குழுவிலகுவதற்கு, பொருள் அல்லது உடலுடன் ஒரு செய்தியை அனுப்பவும்
 'உதவி'
 $ request_email

 பட்டியலை நிர்வகிக்கும் நபரை நீங்கள் அடையலாம்
 $ உரிமையாளர்_மெயில்

 பதிலளிக்கும் போது, தயவுசெய்து உங்கள் பொருள் வரியைத் திருத்தவும், எனவே இது மிகவும் குறிப்பிட்டது
 "மறு: $ டிச்ப்ளே_பேம் டைசெச்டின் உள்ளடக்கங்கள் ..." ___________________________________________
 $ display_name அஞ்சல் பட்டியல் - $ பட்டியல் பெயர்
 குழுவிலகுவதற்கு $ {short_listname} -leave@$ {domain} க்கு ஒரு மின்னஞ்சலை அனுப்பவும் உங்கள் முகவரி "$ USER_EMAIL" $ short_listname இல் சேர அழைக்கப்பட்டுள்ளது
 $ shord_listname அஞ்சல் பட்டியல் உரிமையாளர் மூலம் $ டொமைனில் அஞ்சல் பட்டியல்.
 இந்த செய்திக்கு வெறுமனே பதிலளிப்பதன் மூலம் நீங்கள் அழைப்பை ஏற்கலாம்.

 அல்லது நீங்கள் பின்வரும் வரியை சேர்க்க வேண்டும் - பின்வருவனவற்றை மட்டுமே
 வரி - $ request_email க்கு ஒரு செய்தியில்:

 உறுதிப்படுத்தவும் $ டோக்கன்

 இந்த செய்திக்கு ஒரு `பதிலை 'அனுப்புவது வேலை செய்ய வேண்டும் என்பதை நினைவில் கொள்க
 பெரும்பாலான அஞ்சல் வாசகர்கள்.

 இந்த அழைப்பை நீங்கள் மறுக்க விரும்பினால், தயவுசெய்து இதை புறக்கணிக்கவும்
 செய்தி. உங்களிடம் ஏதேனும் கேள்விகள் இருந்தால், தயவுசெய்து அவற்றை அனுப்புங்கள்
 $ உரிமையாளர்_மெயில். மின்னஞ்சல் முகவரி பதிவு உறுதிப்படுத்தல்

 வணக்கம், இது $ டொமைனில் குனு மெயில்மேன் சேவையகம்.

 மின்னஞ்சல் முகவரிக்கு பதிவு கோரிக்கையை நாங்கள் பெற்றுள்ளோம்

 $ user_email

 இந்த தளத்தில் குனு மெயில்மேனைப் பயன்படுத்தத் தொடங்குவதற்கு முன், நீங்கள் முதலில் உறுதிப்படுத்த வேண்டும்
 இது உங்கள் மின்னஞ்சல் முகவரி என்று. இந்த செய்திக்கு பதிலளிப்பதன் மூலம் இதைச் செய்யலாம்.

 அல்லது நீங்கள் பின்வரும் வரியை சேர்க்க வேண்டும் - பின்வருவனவற்றை மட்டுமே
 வரி - $ request_email க்கு ஒரு செய்தியில்:

 உறுதிப்படுத்தவும் $ டோக்கன்

 இந்த செய்திக்கு ஒரு `பதிலை 'அனுப்புவது வேலை செய்ய வேண்டும் என்பதை நினைவில் கொள்க
 பெரும்பாலான அஞ்சல் வாசகர்கள்.

 இந்த மின்னஞ்சல் முகவரியை நீங்கள் பதிவு செய்ய விரும்பவில்லை என்றால், இதை புறக்கணிக்கவும்
 செய்தி. நீங்கள் பட்டியலில் தீங்கிழைக்கும் வகையில் குழுசேர்கிறீர்கள் என்று நீங்கள் நினைத்தால், அல்லது
 வேறு ஏதேனும் கேள்விகள் உள்ளன, நீங்கள் தொடர்பு கொள்ளலாம்

 $ உரிமையாளர்_மெயில் மின்னஞ்சல் முகவரி குழுவிலக உறுதிப்படுத்தல்

 வணக்கம், இது $ டொமைனில் குனு மெயில்மேன் சேவையகம்.

 மின்னஞ்சல் முகவரிக்கு ஒரு குழுவிலகும் கோரிக்கையை நாங்கள் பெற்றுள்ளோம்

 $ user_email

 குனு மெயில்மேன் உங்களை குழுவிலகுவதற்கு முன், முதலில் உங்கள் கோரிக்கையை உறுதிப்படுத்த வேண்டும்.
 இந்த செய்திக்கு வெறுமனே பதிலளிப்பதன் மூலம் இதைச் செய்யலாம்.

 அல்லது நீங்கள் பின்வரும் வரியை சேர்க்க வேண்டும் - பின்வருவனவற்றை மட்டுமே
 வரி - $ request_email க்கு ஒரு செய்தியில்:

 உறுதிப்படுத்தவும் $ டோக்கன்

 இந்த செய்திக்கு ஒரு `பதிலை 'அனுப்புவது வேலை செய்ய வேண்டும் என்பதை நினைவில் கொள்க
 பெரும்பாலான அஞ்சல் வாசகர்கள்.

 இந்த மின்னஞ்சல் முகவரியை குழுவிலக நீங்கள் விரும்பவில்லை என்றால், இதை புறக்கணிக்கவும்
 செய்தி. நீங்கள் பட்டியலில் இருந்து தீங்கிழைக்கும் வகையில் குழுவிலகப்படுகிறீர்கள் என்று நீங்கள் நினைத்தால்,
 அல்லது வேறு ஏதேனும் கேள்விகள் உள்ளன, நீங்கள் தொடர்பு கொள்ளலாம்

 $ உரிமையாளர்_மெயில் இந்த விசயத்துடன் '$ பட்டியல் பெயர்' என்ற உங்கள் அஞ்சல்

 $ பொருள்

 பட்டியல் மதிப்பீட்டாளர் அதை ஒப்புதலுக்காக மதிப்பாய்வு செய்யும் வரை நடைபெறுகிறது.

 செய்தி நடைபெறுகிறது:

 $ காரணங்கள்

 ஒன்று செய்தி பட்டியலில் வெளியிடப்படும், அல்லது நீங்கள் பெறுவீர்கள்
 மதிப்பீட்டாளரின் முடிவின் அறிவிப்பு. உங்கள் முகவரியிலிருந்து ஒரு செய்தியைப் பெற்றுள்ளோம் <$ sender_email> ஒரு கோருகிறது
 $ பட்டியல் பெயர் அஞ்சல் பட்டியலிலிருந்து தானியங்கி பதில்.

 இன்று நாம் பார்த்த எண்: $ எண்ணிக்கை. போன்ற சிக்கல்களைத் தவிர்ப்பதற்காக
 மின்னஞ்சல் ரோபோக்களுக்கு இடையில் அஞ்சல் சுழல்கள், நாங்கள் உங்களுக்கு மேலும் அனுப்ப மாட்டோம்
 இன்று பதில்கள். நாளை மீண்டும் முயற்சிக்கவும்.

 இந்த செய்தி பிழையாக இருப்பதாக நீங்கள் நம்பினால், அல்லது உங்களுக்கு ஏதேனும் கேள்விகள் இருந்தால், தயவுசெய்து
 பட்டியல் உரிமையாளரை $ உரிமையாளர்_மெயில் தொடர்பு கொள்ளவும். உங்கள் செய்தி

 $ பொருள்

 $ display_name அஞ்சல் பட்டியல் மூலம் வெற்றிகரமாக பெறப்பட்டது. இது ஒரு ஆய்வு செய்தி. இந்த செய்தியை நீங்கள் புறக்கணிக்கலாம்.

 $ பட்டியல் பெயர் அஞ்சல் பட்டியல் உங்களிடமிருந்து பல பவுன்ச் பெற்றுள்ளது,
 $ sender_email க்கு செய்திகளை வழங்குவதில் சிக்கல் இருக்கலாம் என்பதைக் குறிக்கிறது. A
 மாதிரி கீழே இணைக்கப்பட்டுள்ளது. இருப்பதை உறுதிப்படுத்த இந்த செய்தியை ஆராயுங்கள்
 உங்கள் மின்னஞ்சல் முகவரியில் எந்த பிரச்சனையும் இல்லை. உங்கள் அஞ்சலுடன் சரிபார்க்க விரும்பலாம்
 மேலும் உதவிக்கு நிர்வாகி.

 அஞ்சல் பட்டியலில் இயக்கப்பட்ட உறுப்பினராக இருக்க நீங்கள் எதுவும் செய்ய தேவையில்லை.

 உங்களிடம் ஏதேனும் கேள்விகள் அல்லது சிக்கல்கள் இருந்தால், நீங்கள் அஞ்சல் பட்டியல் உரிமையாளரை தொடர்பு கொள்ளலாம்
 at

 $ உரிமையாளர்_மெயில் $ பட்டியல் பெயர் அஞ்சல் பட்டியலுக்கான உங்கள் கோரிக்கை

 $ கோரிக்கை

 பட்டியல் மதிப்பீட்டாளரால் நிராகரிக்கப்பட்டுள்ளது. மதிப்பீட்டாளர் கொடுத்தார்
 உங்கள் கோரிக்கையை நிராகரிப்பதற்கான காரணத்தைத் தொடர்ந்து:

 "$ காரணம்"

 ஏதேனும் கேள்விகள் அல்லது கருத்துகள் பட்டியல் நிர்வாகிக்கு அனுப்பப்பட வேண்டும்
 at:

 $ உரிமையாளர்_மெயில் $ பட்டியல் பெயர் அஞ்சல்-பட்டியலுக்கான உங்கள் செய்தி பின்வருவனவற்றிற்காக நிராகரிக்கப்பட்டது
 காரணங்கள்:

 $ காரணங்கள்

 மெயில்மேன் பெற்ற அசல் செய்தி இணைக்கப்பட்டுள்ளது. உங்கள் சந்தா $ பட்டியல் பெயர் அஞ்சல் பட்டியலில் முடக்கப்பட்டுள்ளது, ஏனெனில் அது உள்ளது
 வழங்குவதில் சிக்கல் இருக்கலாம் என்பதைக் குறிக்கும் பல பவுன்களைப் பெற்றது
 $ sender_email க்கு செய்திகள். உங்கள் அஞ்சல் நிர்வாகியுடன் நீங்கள் சரிபார்க்க விரும்பலாம்
 மேலும் உதவிக்கு.

 உங்களிடம் ஏதேனும் கேள்விகள் அல்லது சிக்கல்கள் இருந்தால், நீங்கள் அஞ்சல் பட்டியல் உரிமையாளரை தொடர்பு கொள்ளலாம்

 $ உரிமையாளர்_மெயில் "$ Display_name" அஞ்சல் பட்டியலுக்கு வருக!

 இந்த பட்டியலில் இடுகையிட, உங்கள் செய்தியை இதற்கு அனுப்பவும்:

 $ பட்டியல் பெயர்

 அனுப்புவதன் மூலம் மின்னஞ்சல் வழியாக உங்கள் விருப்பங்களில் நீங்கள் குழுவிலகலாம் அல்லது மாற்றங்களைச் செய்யலாம்
 இதற்கு ஒரு செய்தி:

 $ request_email

 பொருள் அல்லது உடலில் 'உதவி' என்ற வார்த்தையுடன் (மேற்கோள்களை சேர்க்க வேண்டாம்), மற்றும்
 அறிவுறுத்தல்களுடன் ஒரு செய்தியை நீங்கள் திரும்பப் பெறுவீர்கள். உங்கள் கடவுச்சொல் உங்களுக்குத் தேவைப்படும்
 உங்கள் விருப்பங்களை மாற்றவும், ஆனால் பாதுகாப்பு நோக்கங்களுக்காக, இந்த கடவுச்சொல் சேர்க்கப்படவில்லை
 இங்கே. உங்கள் கடவுச்சொல்லை நீங்கள் மறந்துவிட்டால், அதை மீட்டமைக்க வேண்டும்
 வலை இடைமுகம். இதற்கில்லை கிடைக்கவில்லை ரீட்லைன் கிடைக்கவில்லை துண்டு மற்றும் வரம்பு முழு எண்களாக இருக்க வேண்டும்: $ {value} {} 