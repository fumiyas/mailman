��    �      �  �   �	           	          &  ;   >  �   z     :  '   L  s   t     �  C   �     ?  9   ]  �   �       t   3  �   �     �  <   �  8   �  :   "  0   ]  /   �  1   �  @   �     1  %   K  8   q     �     �     �      �       (   %  (   N     w  .   �  '   �     �     �  7     $   E      j  ,   �     �     �  &   �     �  3     	   J  /   T  $   �     �     �     �        !      #   B  m  f  (   �     �  '        E  (   ]     �     �     �  %   �     �  *   �  "      )   C     m      �  #   �     �  *   �          1     B     Z     ^  Q   n  A   �  S        V     p     �  .   �  #   �      �           <     S     m     �     �  %   �  %   �     �           #     D  0   Q  !   �  ,   �     �  2   �       '   5     ]     i     �      �     �     �     �  1         4      I      b   }   �   )   �       )!  1   J!  %   |!  "   �!     �!     �!     �!  '   "     7"  	   N"  =   X"  2   �"  L   �"  F   #  R   ]#  S   �#  @   $  J   E$  !   �$     �$     �$     �$     �$     %     ,%     K%     e%     �%     �%     �%     �%  �  �%     ~'     �'     �'  G   �'  �   (     �(  /   )  �   8)     �)  L   �)      *  =   >*  �   |*     "+  �   :+  R  �+     -  C   7-  ?   {-  =   �-  2   �-  %   ,.  '   R.  ?   z.     �.  )   �.  6    /     7/     D/     b/     w/      �/  4   �/  /   �/     0  1   00  -   b0     �0     �0  P   �0  %   1  "   61  1   Y1     �1     �1  ,   �1     �1  @   2  	   D2  M   N2  "   �2  #   �2  &   �2  $   
3  /   /3  3   _3  -   �3  �  �3  &   �5  $   �5  )   �5  (   �5  0   !6  #   R6     v6      �6  (   �6  
   �6  /   �6  *   7  :   67     q7  &   �7  #   �7     �7  :   �7  )   ,8     V8     g8     �8     �8  N   �8  >   �8  P   $9     u9     �9  +   �9  ?   �9     :  *   9:  #   d:  "   �:  %   �:     �:     �:     ;  )   ;  )   =;     g;     };     �;     �;  6   �;     �;  D   <     ]<  B   z<  $   �<  7   �<     =  '   (=      P=  #   q=     �=     �=     �=  D   �=     >  $   1>  "   V>  �   y>  (   �>  %   $?  5   J?  2   �?  *   �?     �?     �?  &   @  ,   4@     a@     u@  H   �@  :   �@  O   	A  :   YA  `   �A  [   �A  X   QB  P   �B     �B     C     (C     :C  "   LC  �  oC  �   E  Z   �E  !  F  �   :G  {  �G  Q  KI     �K        S   ]          c       $   g       T                  �   m   ;   M   x   >   ?   �   C              @      �          �      O   a   9          }   �          !   
   q          I   )           ^   :   �       �       �   F          �       y      w       |       +   D   o   7   "       u   j   2           �   H   #              5   Z   k   �   s         (           V   *   L   �   �   &   R       X   Q   0   �       z   E      U          �            `   	   p   {          �       d   �       r       Y   P       [       �       <   ~   ,       8       �   _   '          t      J       3   =   �          4                                      �   N       W      v       G   �       A   l   �   n   K       b   f               6   �   �   %         1      /   .   e   �       B   i   �   h   \   -    
- Results: 
Held Messages:
 
Held Unsubscriptions:
     Add and/or delete owners and moderators of a list.
         Configuration file to use.  If not given, the environment variable
    MAILMAN_CONFIG_FILE is consulted and used if set.  If neither are given, a
    default configuration file is loaded.     Date: ${date}     Delete members from a mailing list.     Email address of the user to be removed from the given role.
    May be repeated to delete multiple users.
         From: ${from_}     If you are sure you want to run as root, specify --run-as-root.     Message-ID: ${message_id}     Override the list's setting for send_welcome_message.     Running mailman commands as root is not recommended and mailman will
    refuse to run as root unless this option is specified.     Subject: ${subject}     The role to add/delete. This may be 'owner' or 'moderator'.
    If not given, then 'owner' role is assumed.
         User to add with the given role. This may be an email address or, if
    quoted, any display name and email address parseable by
    email.utils.parseaddr. E.g., 'Jane Doe <jane@example.com>'. May be repeated
    to add multiple users.
      (Digest mode) ${email} is already a ${role.name} of ${mlist.fqdn_listname} ${email} is not a ${role.name} of ${mlist.fqdn_listname} ${member}'s subscription disabled on ${mlist.display_name} ${mlist.display_name} mailing list probe message ${mlist.display_name} subscription notification ${mlist.display_name} unsubscription notification ${mlist.fqdn_listname} post from ${msg.sender} requires approval ${name} runs ${classname} ${realname} via ${mlist.display_name} ${self.name}: too many arguments: ${printable_arguments} (no subject) - Original message details: Accept a message. Add the message to the archives. Address changed from {} to {}. Address {} already exists; can't change. Address {} is not a valid email address. Address {} not found. After content filtering, the message was empty Already subscribed (skipping): ${email} An alias for 'end'. An alias for 'join'. Cannot parse as valid email address (skipping): ${line} Confirmation email sent to ${person} Confirmation token did not match Decorate a message with headers and footers. Digest Footer Digest Header Discard a message and stop processing. Display Mailman's version. Display more debugging information to the log file. Email: {} Emergency moderation is in effect for this list Filter the MIME content of messages. Forward of moderated message GNU Mailman is already running GNU Mailman is not running Get a list of the list members. Header "{}" matched a header rule Hold a message and stop processing. If you reply to this message, keeping the Subject: header intact, Mailman will
discard the held message.  Do this if the message is spam.  If you reply to
this message and include an Approved: header with the list password in it, the
message will be approved for posting to the list.  The Approved: header can
also appear in the first line of the body of the reply. Information about this Mailman instance. Invalid email address: ${email} Invalid language code: ${language_code} Join this mailing list. Last autoresponse notification for today Leave this mailing list. Less verbosity List all mailing lists. List already exists: ${fqdn_listname} List: {} Member not subscribed (skipping): ${email} Members of the {} mailing list:
{} Membership is banned (skipping): ${email} Message contains administrivia Message has implicit destination Message has more than {} recipients Message has no subject Message sender {} is banned from this list Missing data for request {}

 Moderation chain Modify message headers. N/A Name: ${fname}
 New subscription request to ${self.mlist.display_name} from ${self.address.email} New unsubscription request from ${mlist.display_name} by ${email} New unsubscription request to ${self.mlist.display_name} from ${self.address.email} No child with pid: ${pid} No confirmation token found No matching mailing lists found No registered user for email address: ${email} No sender was found in the message. No such command: ${command_name} No such list found: ${spec} No such list: ${_list} No such list: ${listspec} Nothing to add or delete. Nothing to do Original Message PID unreadable in: ${config.PID_FILE} Print detailed instructions and exit. Print less output. Print some additional status. Print the Mailman configuration. Reason: {}

 Regenerate the aliases appropriate for your MTA. Regular expression requires --run Reject/bounce a message and stop processing. Remove a mailing list. Request to mailing list "${display_name}" rejected Send automatic responses. Send the message to the outgoing queue. Sender: {}
 Show also the list descriptions Show also the list names Show this help message and exit. Stale pid file removed. Stop processing commands. Subject: {}
 Subscription already pending (skipping): ${email} Subscription request Suppress status messages The built-in moderation chain. The content of this message was lost. It was probably cross-posted to
multiple lists and previously handled on another list.
 The message comes from a moderated member The message has no valid senders The message is larger than the {} KB maximum size The message is not from a list member The results of your email commands Today's Topics:
 Uncaught bounce notification Undefined domain: ${domain} Unrecognized or invalid argument(s):
{} Unsubscription request User: {}
 Welcome to the "${mlist.display_name}" mailing list${digmode} You are not authorized to see the membership list. You have been invited to join the ${event.mlist.fqdn_listname} mailing list. You have been unsubscribed from the ${mlist.display_name} mailing list Your confirmation is needed to join the ${event.mlist.fqdn_listname} mailing list. Your confirmation is needed to leave the ${event.mlist.fqdn_listname} mailing list. Your message to ${mlist.fqdn_listname} awaits moderator approval Your subscription for ${mlist.display_name} mailing list has been disabled [No bounce details are available] [No details are available] [No reason given] [No reasons given] bad argument: ${argument} list:member:digest:masthead.txt list:member:generic:footer.txt list:user:notice:post.txt list:user:notice:refuse.txt list:user:notice:rejected.txt list:user:notice:warning.txt list:user:notice:welcome.txt n/a Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-12-23 23:12+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>
Language-Team: Albanian <https://hosted.weblate.org/projects/gnu-mailman/mailman/sq/>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.10-dev
 
- Përfundime: 
Mesazhe të Mbajtur:
 
Pajtime të Mbajtur:
     Shtoni dhe/ose hiqni anëtarë dhe moderatorë të një liste.
         Kartelë formësimi për t’u përdorur.  Nëse s’jepet, shihet për ndryshoren
    e mjedisit MAILMAN_CONFIG_FILE dhe përdoret, në pastë.  Nëse s’jepet as
    njëra, as tjetra, ngarkohet një kartelë parazgjedhje formësimi.     Datë: ${date}     Fshini anëtarë nga një listë postimesh.     Adresë email e përdoruesit që i duhet hequr roli i dhënë.
    Mund të përsëritet, për të hequr përdorues të tjerë.
         Nga: ${from_}     Nëse jeni i sigurt se doni ta xhironi si rrënjë, jepni --run-as-root.     ID Mesazhi: ${message_id}     Anashkalo rregullimin e listë për send_welcome_message.     S’rekomandohet xhirimi i urdhrave të mailman-it si rrënjë dhe mailman-i
    s’do të pranojë të xhirohet nga rrënjë, veç në u dhëntë kjo mundësi.     Subjekt: ${subject}     Roli për shtim/heqje. Ky mund të jetë “i zoti”, ose “moderator”.
    Nëse s’jepet, atëherë merret si roli “i zoti”.
         Përdorues për t’u shtuar me rolin e dhënë. Kjo mund të jetë një adresë
    email, ose, nëse vihet brenda thonjëzash, çfarëdo emri në ekran dhe adresë
    email që mund të analizohet nga email.utils.parseaddr. P.sh., 'Jane Doe
    <jane@example.com>'. Mund të përsëritet për të shtuar të tjerë përdorues.
      (Mënyrë përmbledhje) ${email} është tashmë një ${role.name} i ${mlist.fqdn_listname} ${email} s’është një ${role.name} i ${mlist.fqdn_listname} Pajtimi për ${member} te ${mlist.display_name} u çaktivizua Mesazh prove liste postimesh ${mlist.display_name} njoftim pajtimi ${mlist.display_name} njoftim shpajtimi ${mlist.display_name} Postimi te ${mlist.fqdn_listname} nga ${msg.sender} lyp miratim ${name} xhiron ${classname} ${realname} përmes ${mlist.display_name} ${self.name}: shumë argumente: ${printable_arguments} (pa subjekt) - Hollësi mesazhi origjinal: Pranoni një mesazh. Shtoji mesazhet te arkivat. Adresa u ndryshua nga {} në {}. Ka tashmë një adresë {}; s’mund të ndryshohet. Adresa {} s’është adresë email e vlefshme. S’u gjet adresë {}. Pas filtrimit të lëndës, mesazhi qe i zbrazët I pajtuar tashmë (po anashkalohet): ${email} Një alias për 'end'. Një alias për 'join'. S’mund të analizohet një adresë email e vlefshme (po anashkalohet): ${line} U dërgua email ripohimi te ${person} Token-i i ripohimit s’u përputh Zbukurojeni një mesazh me kryefaqe dhe fundfaqe. Poshtëshënim Përmbledhjeje Krye Përmbledhjeje Hidhe tej një mesazh dhe ndale përpunimin. Shfaq version Mailman-i. Shfaq më tepër të dhëna diagnostikimi te kartela regjistër. Email: {} Për këtë listë është në fuqi moderim nën gjendje të jashtëzakonshme Filtro lëndë MIME të mesazheve. Përcjellje e mesazhit të moderuar GNU Mailman është në xhirim tashmë GNU Mailman s’është duke xhiruar Merrni një listë të anëtarëve të listës. Kryet “{}” kanë përkim me një rregull kryesh Mbaj pezull një mesazh dhe ndal përpunimin. Nëse i përgjigjeni këtij mesazhi, mbajtja të paprekura e kryeve “SubjeKt:”
do të sjellë hedhjen tej nga Mailman-i të mesazhit të mbajtur. Bëjeni këtë
nëse mesazhi është i padëshiruar. Nëse përgjigjeni te ky mesazh dhe përfshini
kryet “I miratuar:” me fjalëkalimin e listë në të, mesazhi do të miratohet
për postim te lista. Kryet “I miratuar:” mund të shfaqet edhe te rreshti
i parë i lëndës së përgjigjes. Hollësi mbi këtë instancë Mailman. Adresë email e pavlefshme: ${email} Kod gjuhe i pavlefshëm: ${language_code} Bëhuni pjesë e kësaj liste postimesh. Njoftimi i fundit për vetëpërgjigje, për sot Braktiseni këtë listë postimesh. Me pak hollësi Radhit krejt listat e postimeve. Lista ekziston tashmë: ${fqdn_listname} Listë: {} Anëtar i papajtuar (po anashkalohet): ${email} Anëtarë të listës së postimeve {}:
{} Anëtarësia është e ndaluar (po anashkalohet): ${email} Mesazhi përmban administrivia Mesazhi ka vendmbërritje shprehimisht Mesazhi ka më tepër se {} marrës Mesazh s’ka subjekt Dërguesi {} i mesazhi është i dëbuar prej kësaj liste Mungojnë të dhëna për kërkesën {}

 Zinxhir moderimi Ndryshoni krye mesazhesh. N/A Emër: ${fname}
 Kërkesë e re pajtimi te ${self.mlist.display_name} nga ${self.address.email} Kërkesë e re pajtimi prej ${mlist.display_name} nga ${email} Kërkesë e re shpajtimi te ${self.mlist.display_name} nga ${self.address.email} S’ka pjellë me pid: ${pid} S’u gjet token ripohimi S’u gjetën lista postimesh me përputhje S’ka përdorues të regjistruar për adresën email: ${email} Te mesazhi s’u gjet dërgues. S’ka urdhër të tillë: ${command_name} S’u gjet listë e tillë: ${spec} S’ka listë të tillë: ${_list} S’ka listë të tillë: ${listspec} Asgjë për shtim ose heqje. S’ka ç’bëhet Mesazhi Origjinal PID i palexueshëm te: ${config.PID_FILE} Shtyp udhëzime të hollësishme dhe dil. Shtyp më pak gjëra. Shtyp ca gjendje shtesë. Shtyp formësimin e Mailman-it. Arsye: {}

 Riprodhoni aliaset e përshtatshëm për MTA-në tuaj. Shprehja e rregullt lyp --run Hidhni poshtë/riktheni mbrapsht një mesazh dhe ndalni përpunimin. Hiqni një listë postimesh. Kërkesa te lista e postimeve “${display_name}” u hodh poshtë Dërgo përgjigje të automatizuara. Dërgoje mesazhin te radha e atyre për t’u dërguar. Dërgues: {}
 Shfaq gjithashtu përshkrimet e listave Shfaq gjithashtu emrat e listave Shfaq këtë mesazh ndihme dhe dil. U hoq kartelë pid e vjetruar. Ndal përpunim urdhrash. Subjekt: {}
 Pajtim tashmë në pritje të shqyrtimit (po anashkalohet): ${email} Kërkesë pajtimi Mos lejo shfaqje mesazhesh gjendjeje Zinxhiri i brendshëm i moderimit. Lënda e këtij mesazhi humbi. Ka shumë mundësi të qe postuar
në disa lista dhe trajtuar më herët në një listë tjetër.
 Mesazhi vjen nga një anëtar i moderuar Mesazhi s’ka dërgues të vlefshëm Mesazhi është më i madh se madhësi maksimum {} KB Mesazhi s’është prej një anëtari të listës Përfundimet për urdhrat tuaja për email Temat e Sotme:
 Njoftim kthimi që s’u pikas Përkatësi e papërcaktuar: ${domain} Argument(e) i panjohur ose i pavlefshëm:
{} Kërkesë shpajtimi Përdorues: {}
 Mirë se vini te lista e postimeve “${mlist.display_name}”${digmode} S’jeni i autorizuar të shihni listën e anëtarësisë. Jeni ftuar të merrni pjesë te lista e postimeve ${event.mlist.fqdn_listname}. Jeni shpajtuar nga lista e postimeve ${mlist.display_name} Që të merrni pjesë te lista e postimeve ${event.mlist.fqdn_listname}, lypset ripohimi nga ju. Që të braktisni listën e postimeve ${event.mlist.fqdn_listname}, lypset ripohimi nga ju. Mesazhi juaj te ${mlist.fqdn_listname} është në pritje të miratimit nga moderatorët Pajtimi juaj për listën e postimeve ${mlist.display_name} është çaktivizuar [S’ka hollësi kthimesh] [S’ka hollësi] [S’u dha arsye] [S’u dha arsye] argument i keqformuar: ${argument} Dërgoni parashtrime liste postimesh $display_name te
	$listname

Që të pajtoheni, apo shpajtoheni përmes email-i, dërgoni një mesazh me “help” te
subjekti apo lënda, te
	$request_email

Me personin që administron listën mund të lidheni përmes
	$owner_email

Kur përgjigjeni, ju lutemi, përpunoni rreshtin e Subjektit tuaj, që të jetë
më specifik se sa
“Re: Contents of $display_name digest…” _______________________________________________
Lista e postimeve $display_name -- $listname
Që të shpajtoheni, dërgoni një email te ${short_listname}-leave@${domain} Mesazhi juaj me titull

    $subject

u mor me sukses nga lista e postimeve $display_name. Kërkesa juaj te lista e postimeve $listname

    $request

është hedhur poshtë nga moderatori i listës.  Moderatori dha
arsyen vijues për hedhjen poshtë të kërkesës suaj:

"$reason"

Çfarëdo pyejtesh apo komentesh duhen drejtuar te përgjegjësi i listës
te:

    $owner_email Mesazhi juaj te lista e postimeve $listname u hodh poshtë për arsyet
vijuese:

$reasons

Bashkëngjitet mesazhi origjinal, siç u mor nga Mailman. Pajtimi juaj te lista e postimeve $listname pështë çaktivizuar, ngaqë për të
ka pasur një numër kthimesh që tregojnë se mund të ketë ndonjë problem dërgimi
mesazhesh te $sender_email. Për më tepër ndihmë, mund të doni të shihni me
përgjegjësin tuaj të email-it.

Nëse keni pyetje apo probleme, mund të lidheni me të zotin e listës, te

    $owner_email Mirë se vini te lista e postimeve “$display_name”!

Që të postoni te kjo listë, dërgojeni mesazhin tuaj te:

  $listname

Mund të shpajtoheni, ose të bëni përimtime te mundësitë
tuaja përmes email-i, duke dërguar një mesazh te:

  $request_email

me fjalën “help” te subjekti ose lënda (pa thonjëzat) dhe do të merrni një
mesazh me udhëzime.  Do t’ju duhet fjalëkalimi juaj që të ndryshoni mundësitë
tuaja, por për arsye sigurie, ky fjalëkalim s’tregohet këtu. Nëse e keni
harruar fjalëkalimin tuaj, do t’ju duhet ta ricaktoni përmes ndërfaqes UI. n/a 